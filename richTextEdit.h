#ifndef richTextEditH
#define richTextEditH
//--------------------------------------------------------------------------
#include <QtGui>
#include <QAction>
#include "ui_richTextEdit.h"

//#include "docElement.h"
class TdocElement;
//--------------------------------------------------------------------------
class TrichTextEdit : public  QWidget, public Ui::RichTextEdit
{
 Q_OBJECT
 protected:
//  QMenuBar * mb; QMenu * mfile;
  QAction * aBold, * aItalic, * aGet;

// private:
//  TrichTextEdit    * lw;
//  QString      fileName;
//  QDomDocument domDoc;

  TdocElement * currentDoc;

 void appendDocElement(TdocElement *);

public:

 TrichTextEdit(QWidget * pwgt = 0);
 ~TrichTextEdit();

 void setupUi(QWidget * p);
 void setDocElement(TdocElement *);

 TdocElement * root;
 bool changed;

 void updateDoc();
signals:
// void textChanged();
 void changedCurrentDoc(TdocElement * ); 
private slots:
 void onTextChanged();
 void onCursorPositionChanged();
 void onBoldPressed();
 void onItalicPressed();
 void onGetPressed();
};
//--------------------------------------------------------------------------
#endif
//--------------------------------------------------------------------------
