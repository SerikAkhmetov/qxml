#ifndef xmlTextEditH
#define xmlTextEditH
//--------------------------------------------------------------------------
#include <QSyntaxHighlighter>
//--------------------------------------------------------------------------
class TblockTextElement;
//--------------------------------------------------------------------------
//class TxmlTextEdit : public QTextEdit
//{
// Q_OBJECT
// private:
//  TdocElement * docElement; 
//};
//--------------------------------------------------------------------------
class TxmlSyntaxHighlighter : public QSyntaxHighlighter
{
 Q_OBJECT

 private:
  TblockTextElement * textElement;

 protected:
 virtual void highlightBlock(const QString & text);

 public:
 TxmlSyntaxHighlighter(TblockTextElement * doc, QTextDocument * p = 0) : QSyntaxHighlighter(p)
 {textElement = doc;};

// virtual void setDocElement(TdocElement * p){docElement = p;}
};
//--------------------------------------------------------------------------
#endif
