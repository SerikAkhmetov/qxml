#ifndef xmlDocH
#define xmlDocH
//--------------------------------------------------------------------------
#include <QtGui>
#include <QAction>
#include <QtXml>
#include <QStandardItem>
#include <QStandardItemModel>
//#include <QMap>
#include "docElement.h"
#include "ui_xmlDoc.h"
//--------------------------------------------------------------------------
class TxmlTreeView : public QTreeView
{
Q_OBJECT
public:
 TxmlTreeView(QWidget * parent) : QTreeView(parent)
 {};
protected:
 virtual void mousePressEvent(QMouseEvent * event)
 {
  QTreeView::mousePressEvent(event);
  if(event->button() == Qt::RightButton)
  {
   //qDebug() << "mousePressEvent";
   emit mousePressButtonRight(event->globalPos());
  }
 };
 virtual void mouseDoubleClickEvent(QMouseEvent * event)
 {
// пока не надо обрабатывать
 };
 virtual void keyPressEvent(QKeyEvent * event)
 {
  QTreeView::keyPressEvent(event);
  emit keyPress(event);
 }
 
public:
signals:
 void mousePressButtonRight(QPoint);
 void keyPress(QKeyEvent * event);
};
//--------------------------------------------------------------------------
class TxmlDoc : public  QWidget, public Ui::XmlDoc
{
 Q_OBJECT
 protected:
  QMenuBar * mb; QMenu * mfile;
  QAction * file_open, * file_save, * file_save_as, * file_exit;

  QList<TdocElement *> lBuffer; 

  void setupMainMenu();

 private:
  TxmlDoc    * lw;
  QString      fileName;
  QDomDocument domDoc;

  QStandardItemModel treeModel;
  QItemSelectionModel selModel;

//  void traverseNode(const QDomNode& node, QStandardItem * item);

// QMap<QStandardItem item, QDomNode node> s;
  TdocElement * doc; 

//  QStandardItemDoc * getCurrentItem(void) const;  
  QStandardItemDoc * currentItem;

public:

 TxmlDoc(QWidget * pwgt = 0);
 ~TxmlDoc();

 void setupUi(QWidget * p);
 bool open(const QString p);
 bool save(const QString p);

 private slots:
 void onOpenPressed();
 void onSavePressed();
 void onSaveAsPressed();
 void onSelectionChanged(const QItemSelection &, const QItemSelection &);
 void onMousePressButtonRight(QPoint);
// void onMenuPressed();
 void onTreeKeyPressEvent(QKeyEvent *);

 void onItemCopy();
 void onItemPaste();
 void onItemMove();

 //void onCreateBinary();
 void onCreateImageFromFile();
 void onCreateImageFromClipboard();
 void onImportTextFile();
 void onImportTextFromClipboard();

 public slots:
 void onNewFilePressed();
 void onTreeRefresh();
 void onTreeSetIndex(const QModelIndex&);
 //void onTreeUpItem(QStandardItem *);
 //void onTreeDownItem(QStandardItem *);
 void onTreeAppendItem(TdocElement * root, QStandardItem * item);

 private:
// void keyPressEvent(QKeyEvent * event);
//void doc2model(TdocElement * root, QStandardItem * item);
};
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//void doc2model(TdocElement * root, QStandardItem * item);
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
#endif
