#include <QAction>
#include "docElement.h"
#include "richTextEdit.h"
//--------------------------------------------------------------------------
TrichTextEdit::TrichTextEdit(QWidget * pwgt) : QWidget(pwgt), changed(false)
{
 root       = NULL;
 currentDoc = NULL;
 setupUi(this);
}
//--------------------------------------------------------------------------
TrichTextEdit::~TrichTextEdit()
{
//  qDebug() << "~TrichTextEdit";
 updateDoc();
}
//--------------------------------------------------------------------------
void TrichTextEdit::setupUi(QWidget * p)
{
 Ui::RichTextEdit::setupUi(p);

 setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

 aBold = new QAction("Bold", p);
 bBold->setDefaultAction(aBold);
 connect(aBold, SIGNAL(triggered()), SLOT(onBoldPressed()));
  
 aItalic = new QAction("Italic", p);
 bItalic->setDefaultAction(aItalic);
 connect(aItalic, SIGNAL(triggered()), SLOT(onItalicPressed()));

 aGet = new QAction("Get", p);
 bGet->setDefaultAction(aGet);
 connect(aGet, SIGNAL(triggered()), SLOT(onGetPressed()));
}
//--------------------------------------------------------------------------
void TrichTextEdit::setDocElement(TdocElement * doc)
{
 QList<TdocElement *>::iterator itr;
 root = doc;
 for(itr = root->child.begin(); itr != root->child.end(); ++itr)
 {
  appendDocElement(*itr);
 }
 connect(textEdit, SIGNAL(textChanged()), SLOT(onTextChanged()));
//  connect(textEdit, SIGNAL(textChanged()), SLOT(textChanged()));
 connect(textEdit, SIGNAL(cursorPositionChanged()), SLOT(onCursorPositionChanged()));
}
//--------------------------------------------------------------------------
void TrichTextEdit::appendDocElement(TdocElement * doc)
{
 QTextDocument * document  = textEdit->document();
// QTextCursor * cursor = new QTextCursor(document);
 QList<TdocElement *>::iterator itr;

 if ( !doc->getValue().isEmpty() )
 {
//  QTextBlock * block = new QTextBlock();
//  block->setUserData(new QTextBlockUserDataDoc(doc));
//  block->layout()->setText(doc->getValue());
//  cursor->insertText(doc->getValue());
  textEdit->append(doc->getValue());
//  cursor->insertBlock(); 
  document->lastBlock().setUserData(new QTextBlockUserDataDoc(doc));
 // block->setUserData(new QTextBlockUserDataDoc(doc));
 }

 for(itr = doc->child.begin(); itr != doc->child.end(); ++itr)
 {
  appendDocElement(*itr);
 }
}
//--------------------------------------------------------------------------
void TrichTextEdit::onTextChanged()
{
// qDebug() << "onTextChanged";
// emit(textChanged());
 changed = true;
}
//--------------------------------------------------------------------------
void TrichTextEdit::onCursorPositionChanged()
{
 QTextBlock block ( textEdit->textCursor().block() ); 

qDebug() << __PRETTY_FUNCTION__;

 if (block.userData() != NULL)
 {
  QTextBlockUserDataDoc * data = dynamic_cast<QTextBlockUserDataDoc *>(block.userData());
  if (data->doc != NULL)
  {
   if (currentDoc != data->doc)
   {
    currentDoc = data->doc;
    emit (changedCurrentDoc(currentDoc));
   }    
  }
 }
}
//--------------------------------------------------------------------------
void TrichTextEdit::onBoldPressed()
{
 QTextCharFormat format;
// QTextCursor     c = textEdit->textCursor();
// QTextDocument * doc = textEdit->document();

 format.setFontWeight(QFont::Bold);

 textEdit->mergeCurrentCharFormat(format);
// qDebug() << "onBoldPressed";
}
//--------------------------------------------------------------------------
void TrichTextEdit::onItalicPressed()
{
 QTextCharFormat format;
// QTextCursor     c = textEdit->textCursor();
// QTextDocument * doc = textEdit->document();

 format.setFontItalic(true);

 textEdit->mergeCurrentCharFormat(format);
// qDebug() << "onItalicPressed";
}
//--------------------------------------------------------------------------
void TrichTextEdit::onGetPressed()
{
// qDebug() << "onGetPressed";
 QTextDocument * doc   = textEdit->document();

 QTextBlock::iterator itb;
 QTextBlock block;
 for(block = doc->begin(); block != doc->end(); block = block.next())
 {
//  if (block.userData() != NULL)
//  {
//   QTextBlockUserDataDoc * data = dynamic_cast<QTextBlockUserDataDoc *>(block.userData());
//   qDebug() << data->doc->getName(); 
//  }
//  else
//  {qDebug() << "block().userData() == NULL";}
  for(itb = block.begin(); !(itb.atEnd()); ++itb)
  {
   QTextFragment fragment = itb.fragment();
   QTextCharFormat format = fragment.charFormat();
//   qDebug() << fragment.text() << " " << (format.fontItalic() ? "Italic ":"")
//                                      << (format.fontWeight() == QFont::Bold ? "Bold" : "");
  }
 }

}
//--------------------------------------------------------------------------
void TrichTextEdit::updateDoc()
{
 QTextDocument * doc   = textEdit->document();
 QTextBlock::iterator itb;
 QTextBlock block;
 QList<TdocElement *> oldchild(root->child); 
 QList<TdocElement *>::iterator itr;

 if (!changed) return;

qDebug() << "TrichTextEdit::updateDoc()";

 root->child.clear();

 for(block = doc->begin(); block != doc->end(); block = block.next())
 {
  TdocElement * e;
  if (block.userData() != NULL)
  {
   QTextBlockUserDataDoc * data = dynamic_cast<QTextBlockUserDataDoc *>(block.userData());
   e = data->doc;
   int i (oldchild.indexOf(e));
   if (i >= 0) oldchild.removeAt(i);
  }
  else
  {
   e = new TpElement();
  }
  e->setValue (block.text());
  root->addChild(e);

  for(itb = block.begin(); !(itb.atEnd()); ++itb)
  {
   QTextFragment fragment = itb.fragment();
   QTextCharFormat format = fragment.charFormat();
//   qDebug() << fragment.text() << " " << (format.fontItalic() ? "Italic ":"")
//                                      << (format.fontWeight() == QFont::Bold ? "Bold" : "");
  }
 }

 for(itr = oldchild.begin(); itr != oldchild.end(); ++itr) { delete(*itr); }
 
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
