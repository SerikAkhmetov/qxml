#ifndef docElementH
#define docElementH
//--------------------------------------------------------------------------
#include <QtGui>
#include <QString>
#include <QList>
#include <QObject>
#include <QStandardItem>
#include <QtXml>
#include <QMap>
//#include "xmlTextEdit.h"
#include "richTextEdit.h"
//--------------------------------------------------------------------------
class TdocElement;
//--------------------------------------------------------------------------
//class QStandardItemXml : public QStandardItem
//{
// public:
//  QDomNode node;
//};
//--------------------------------------------------------------------------
class QStandardItemDoc : public QStandardItem //, public QObject
{
 public:
  TdocElement * doc;
};
//--------------------------------------------------------------------------
class QTextBlockUserDataDoc : public QTextBlockUserData
{
 public:
  TdocElement * doc;
  QTextBlockUserDataDoc(){doc = NULL;};
  QTextBlockUserDataDoc(TdocElement * p){doc = p;};
 ~QTextBlockUserDataDoc(){};
};
//--------------------------------------------------------------------------
class TdocElement : public QObject
{
Q_OBJECT

 public:
  QString name;               // название тэга в xml
  QList<QString> allowParent; // разрешенные типы родителей
  QList<QString> allowChild;  // разрешенные типы детей
  bool only;                  // если верно - запрещено иметь братьев этого же типа
  bool text;                  // если верно - содержит форматированный текст

  TdocElement * parent;
  QList<TdocElement *> child;

  TdocElement * getChild(const QString name);
  TdocElement * getParent(const QString name = "");

  QString value;              // содержимое

  QMap<QString, QString> attribute;

  virtual bool initElement(QDomElement * domElement); // импорт данных из QDomElement *
  virtual void initMap(QDomElement * domElement);
  virtual bool saveElement(QDomDocument * doc, QDomElement * domElement); // экспорт данных в QDomElement *

  QStandardItemDoc * item;

 public:
  TdocElement(const QString name);
  TdocElement(const QString name, const bool only);

  ~TdocElement();

  virtual QString getName()  const {return(name);};
//  virtual void    setValue(QString p) { value = p; };
  virtual QString getValue() const {return(value);};
//  virtual QList<QAction *> getActions();
  
  virtual bool isOnly() const {return(only);};
  virtual bool isText() const {return(text);};

  virtual bool setParent(TdocElement *);
  virtual bool addChild(TdocElement *);
          bool insertChild(TdocElement *src, TdocElement * dst = NULL); // Вставить после
          void removeChild(TdocElement *);  // удалит из списка, объект не удаляется
          void removeChild(const int p);
          bool moveChild(TdocElement * src, TdocElement * new_root); // переместить в другого родителя
          int  childPosition(TdocElement *); // положение
  virtual void clearChild();
          void upChild(TdocElement *);
          void downChild(TdocElement *);
  virtual bool checkAllowParent(const QString typeName);// const;
  virtual bool checkAllowChild(const QString typeName);// const;
  virtual void addAllowParent(const QString p){allowParent.push_back(p);};
  virtual void addAllowChild(const QString p) {allowChild.push_back(p);};

  virtual QWidget * getWidget(QWidget * parent);   // построить widget для редактирования
  virtual QStandardItemDoc * getItem();            // построить item для дерева
  virtual QString   getItemName(){return(getValue().left(30));};

  virtual bool isBlockText() {return (false);};
  virtual bool isFontStyle() {return (false);};
  virtual bool isFormatText(){return (false);};

  virtual QMenu * getItemMenu();
  virtual void onItemMousePressButtonRight(QPoint p);

  public slots:
  virtual void setValue(const QString & p) { value = p; };
  virtual void onTextChanged();
//  virtual void onAction();
  virtual void onMenuPressed();
  virtual void onDelete();
//  virtual void onCopy();
//  virtual void onPaste();

  signals:
  void onTreeSetIndex(const QModelIndex&);
  //void onTreeUpItem(QStandardItem *);
  //void onTreeDownItem(QStandardItem *);
};
//--------------------------------------------------------------------------
class TFictionBookElement : public TdocElement
{
 public:
 TFictionBookElement():TdocElement("FictionBook"){};
};
//--------------------------------------------------------------------------
class TdescriptionElement : public TdocElement
{
 public:
 TdescriptionElement():TdocElement("description"){};
};
//--------------------------------------------------------------------------
class TtitleInfoElement : public TdocElement
{
 public:
 TtitleInfoElement():TdocElement("title-info", true)
 {addAllowParent("description");
  addAllowChild("genre");addAllowChild("author");addAllowChild("book-title");
  addAllowChild("annotation");addAllowChild("keywords");addAllowChild("date");
  addAllowChild("coverpage");addAllowChild("lang");addAllowChild("src-lang");
  addAllowChild("translator");addAllowChild("sequence");
 };
};
//--------------------------------------------------------------------------
class TsrcTitleInfoElement : public TdocElement
{
 public:
 TsrcTitleInfoElement():TdocElement("src-title-info", true){addAllowParent("description");};
};
//--------------------------------------------------------------------------
class TpublishInfoElement : public TdocElement
{
 public:
 TpublishInfoElement():TdocElement("publish-info", true)
 {addAllowParent("description");
 addAllowChild("book-name");addAllowChild("publisher");addAllowChild("city");addAllowChild("year");};
};
//--------------------------------------------------------------------------
class TcustomInfoElement : public TdocElement
{
 public:
 TcustomInfoElement():TdocElement("custom-info", false){addAllowParent("description");};
};
//--------------------------------------------------------------------------
class TgenreElement : public TdocElement
{
 Q_OBJECT
 private:
  class Tgenre{public:QString sname, fname; Tgenre(const QString p1, const QString p3): sname(p1), fname(p3){};};
  QList<Tgenre> genre;
  QComboBox * cbo;
  int match;

 public:
 TgenreElement():TdocElement("genre", false), match(100)
 {addAllowParent("title-info"); addAllowParent("src-title-info"); addAllowChild("none-allow-child");
  genre.clear();
  genre.push_back(Tgenre("", "Unknown"));
  genre.push_back(Tgenre("sf_history", "Alternative history"));
  genre.push_back(Tgenre("sf_action", "Action"));
  genre.push_back(Tgenre("sf_epic", "Epic"));
  genre.push_back(Tgenre("sf_heroic", "Heroic"));
  genre.push_back(Tgenre("sf_detective", "Detective"));
  genre.push_back(Tgenre("sf_cyberpunk", "Cyberpunk"));
  genre.push_back(Tgenre("sf_space", "Space"));
  genre.push_back(Tgenre("sf_social", "Social-philosophical"));
  genre.push_back(Tgenre("sf_horror", "Horror & mystic"));
  genre.push_back(Tgenre("sf_humor", "Humor"));
  genre.push_back(Tgenre("sf_fantasy", "Fantasy"));
  genre.push_back(Tgenre("sf", "Science Fiction "));
  genre.push_back(Tgenre("det_classic", "Classical detectives"));
  genre.push_back(Tgenre("det_police", "Police Stories"));
  genre.push_back(Tgenre("det_action", "Action"));
  genre.push_back(Tgenre("det_irony", "Ironical detectives"));
  genre.push_back(Tgenre("det_history", "Historical detectives"));
  genre.push_back(Tgenre("det_espionage", "Espionage detectives"));
  genre.push_back(Tgenre("det_crime", "Crime detectives"));
  genre.push_back(Tgenre("det_political", "Political detectives"));
  genre.push_back(Tgenre("det_maniac", "Maniacs"));
  genre.push_back(Tgenre("det_hard", "Hard-boiled"));
  genre.push_back(Tgenre("thriller", "Thrillers"));
  genre.push_back(Tgenre("detective", "Detectives "));
  genre.push_back(Tgenre("prose_classic", "Classics prose"));
  genre.push_back(Tgenre("prose_history", "Historical prose"));
  genre.push_back(Tgenre("prose_contemporary", "Contemporary prose"));
  genre.push_back(Tgenre("prose_counter", "Counterculture"));
  genre.push_back(Tgenre("prose_rus_classic", "Russial classics prose"));
  genre.push_back(Tgenre("prose_su_classics", "Soviet classics prose "));
  genre.push_back(Tgenre("love_contemporary", "Contemporary Romance"));
  genre.push_back(Tgenre("love_history", "Historical Romance"));
  genre.push_back(Tgenre("love_detective", "Detective Romance"));
  genre.push_back(Tgenre("love_short", "Short Romance"));
  genre.push_back(Tgenre("love_erotica", "Erotica "));
  genre.push_back(Tgenre("adv_western", "Western"));
  genre.push_back(Tgenre("adv_history", "History"));
  genre.push_back(Tgenre("adv_indian", "Indians"));
  genre.push_back(Tgenre("adv_maritime", "Maritime Fiction"));
  genre.push_back(Tgenre("adv_geo", "Travel & geography"));
  genre.push_back(Tgenre("adv_animal", "Nature & animals"));
  genre.push_back(Tgenre("adventure", "adventure - Other"));
  genre.push_back(Tgenre("child_tale", "Fairy Tales"));
  genre.push_back(Tgenre("child_verse", "Verses"));
  genre.push_back(Tgenre("child_prose", "Prose"));
  genre.push_back(Tgenre("child_sf", "Science Fiction"));
  genre.push_back(Tgenre("child_det", "Detectives & Thrillers"));
  genre.push_back(Tgenre("child_adv", "Adventures"));
  genre.push_back(Tgenre("child_education", "Educational"));
  genre.push_back(Tgenre("children", "children - Other"));
  genre.push_back(Tgenre("poetry", "Poetry"));
  genre.push_back(Tgenre("dramaturgy", "Dramaturgy "));
  genre.push_back(Tgenre("antique_ant", "Antique"));
  genre.push_back(Tgenre("antique_european", "European"));
  genre.push_back(Tgenre("antique_russian", "Old russian"));
  genre.push_back(Tgenre("antique_east", "Old east"));
  genre.push_back(Tgenre("antique_myths", "Myths. Legends. Epos"));
  genre.push_back(Tgenre("antique", "antique - Other"));
  genre.push_back(Tgenre("sci_history", "History"));
  genre.push_back(Tgenre("sci_psychology", "Psychology"));
  genre.push_back(Tgenre("sci_culture", "Cultural science"));
  genre.push_back(Tgenre("sci_religion", "Religious studies"));
  genre.push_back(Tgenre("sci_philosophy", "Philosophy"));
  genre.push_back(Tgenre("sci_politics", "Politics"));
  genre.push_back(Tgenre("sci_business", "Business literature"));
  genre.push_back(Tgenre("sci_juris", "Jurisprudence"));
  genre.push_back(Tgenre("sci_linguistic", "Linguistics"));
  genre.push_back(Tgenre("sci_medicine", "Medicine"));
  genre.push_back(Tgenre("sci_phys", "Physics"));
  genre.push_back(Tgenre("sci_math", "Mathematics"));
  genre.push_back(Tgenre("sci_chem", "Chemistry"));
  genre.push_back(Tgenre("sci_biology", "Biology"));
  genre.push_back(Tgenre("sci_tech", "Technical"));
  genre.push_back(Tgenre("science", "science - Other"));
  genre.push_back(Tgenre("comp_www", "Internet"));
  genre.push_back(Tgenre("comp_programming", "Programming"));
  genre.push_back(Tgenre("comp_hard", "Hardware"));
  genre.push_back(Tgenre("comp_soft", "Software"));
  genre.push_back(Tgenre("comp_db", "Databases"));
  genre.push_back(Tgenre("comp_osnet", "OS & Networking"));
  genre.push_back(Tgenre("computers", "computers - Other"));
  genre.push_back(Tgenre("ref_encyc", "Encyclopedias"));
  genre.push_back(Tgenre("ref_dict", "Dictionaries"));
  genre.push_back(Tgenre("ref_ref", "Reference"));
  genre.push_back(Tgenre("ref_guide", "Guidebooks"));
  genre.push_back(Tgenre("reference", "reference - Other"));
  genre.push_back(Tgenre("nonf_biography", "Biography & Memoris"));
  genre.push_back(Tgenre("nonf_publicism", "Publicism"));
  genre.push_back(Tgenre("nonf_criticism", "Criticism"));
  genre.push_back(Tgenre("design", "Art & design"));
  genre.push_back(Tgenre("nonfiction", "nonfiction - Other"));
  genre.push_back(Tgenre("religion_rel", "Religion"));
  genre.push_back(Tgenre("religion_esoterics", "Esoterics"));
  genre.push_back(Tgenre("religion_self", "Self-improvement"));
  genre.push_back(Tgenre("religion", "religion - Other"));
  genre.push_back(Tgenre("humor_anecdote", "Anecdote (funny stories)"));
  genre.push_back(Tgenre("humor_prose", "Prose"));
  genre.push_back(Tgenre("humor_verse", "Verses"));
  genre.push_back(Tgenre("humor", "humor - Other"));
  genre.push_back(Tgenre("home_cooking", "Cooking"));
  genre.push_back(Tgenre("home_pets", "Pets"));
  genre.push_back(Tgenre("home_crafts", "Hobbies & Crafts"));
  genre.push_back(Tgenre("home_entertain", "Entertaining"));
  genre.push_back(Tgenre("home_health", "Health"));
  genre.push_back(Tgenre("home_garden", "Garden"));
  genre.push_back(Tgenre("home_diy", "Do it yourself"));
  genre.push_back(Tgenre("home_sport", "Sports"));
  genre.push_back(Tgenre("home_sex", "Erotica & sex"));
  genre.push_back(Tgenre("home", "home - Other"));
 };
 virtual QWidget * getWidget(QWidget * parent);

 private slots:
  void onCurrentIndexChanged(int index);
  void onValueChanged(int index);
};
//--------------------------------------------------------------------------
class TpersonElement : public TdocElement
{
 public:
 TpersonElement(const QString name, const bool only):TdocElement(name, only)
 {addAllowParent("title-info"); addAllowParent("src-title-info"); addAllowParent("document-info");
  addAllowChild("first-name"); addAllowChild("middle-name"); addAllowChild("last-name");
  addAllowChild("nickname"); addAllowChild("home-page"); addAllowChild("email"); addAllowChild("id");
 };

 virtual QWidget * getWidget(QWidget * parent);
 virtual QStandardItemDoc * getItem();
};
//--------------------------------------------------------------------------
class TauthorElement : public TpersonElement
{
 public:
 TauthorElement():TpersonElement("author", false){};
};
//--------------------------------------------------------------------------
class TfirstNameElement : public TdocElement
{
 public:
 TfirstNameElement():TdocElement("first-name", true){addAllowParent("author"); addAllowParent("translator");};
};
//--------------------------------------------------------------------------
class TlastNameElement : public TdocElement
{
 public:
 TlastNameElement():TdocElement("last-name", true){addAllowParent("author"); addAllowParent("translator");};
};
//--------------------------------------------------------------------------
class TmiddleNameElement : public TdocElement
{
 public:
 TmiddleNameElement():TdocElement("middle-name", true){addAllowParent("author"); addAllowParent("translator");};
};
//--------------------------------------------------------------------------
class TnicknameElement : public TdocElement
{
 public:
 TnicknameElement():TdocElement("nickname", false){addAllowParent("author"); addAllowParent("translator");};
};
//--------------------------------------------------------------------------
class TemailElement : public TdocElement
{
 public:
 TemailElement():TdocElement("email", false){addAllowParent("author"); addAllowParent("translator");};
};
//--------------------------------------------------------------------------
class ThomepageElement : public TdocElement
{
 public:
 ThomepageElement():TdocElement("home-page", false){addAllowParent("author"); addAllowParent("translator");};
};
//--------------------------------------------------------------------------
class TbookTitleElement : public TdocElement
{
 public:
 TbookTitleElement():TdocElement("book-title", true){addAllowParent("title-info");};
};
//--------------------------------------------------------------------------
class TformatTextElement : public TdocElement
{
 Q_OBJECT
 public:
 TformatTextElement(const QString name, const bool only):TdocElement(name, only) //, changed(false)
 {};
 virtual QWidget * getWidget(QWidget * parent);

 virtual bool isFormatText(){return (true);};

 private:
  TrichTextEdit * rte;
  void appendText(TdocElement *);
//  bool changed;

 private slots:
//  virtual void onTextChanged(){changed = true;};
 void onChangedCurrentDoc(TdocElement * );
// public slots:
//  virtual void onTextChanged();
};
//--------------------------------------------------------------------------
class TannotationElement : public TformatTextElement
{
 public:
 TannotationElement():TformatTextElement("annotation", true)
 {addAllowParent("title-info");
  addAllowChild("p"); addAllowChild("poem"); addAllowChild("cite"); addAllowChild("subtitle"); addAllowChild("table"); addAllowChild("empty-line");
 };
};
//--------------------------------------------------------------------------
class TkeywordsElement : public TdocElement
{
 public:
 TkeywordsElement():TdocElement("keywords", true){addAllowParent("title-info");};
};
//--------------------------------------------------------------------------
class TdateElement : public TdocElement
{
 Q_OBJECT
 private:
//  QDate value;
 protected:
  QDateEdit   * de;
 public:
 TdateElement():TdocElement("date", true)
 {
  text = false;
  addAllowParent("title-info"); addAllowParent("src-title-info"); addAllowParent("document-info");
  addAllowParent("poem");
  addAllowChild("none-allow-child");
 };
 virtual QWidget * getWidget(QWidget * parent);
// virtual QByteArray getData() {return(value);};
//public slots:
// virtual void setValue(const QString & p);
 private slots:
  void onDateChanged(const QDate & p);
  void onDateChangedPressed();
};
//--------------------------------------------------------------------------
class TcoverpageElement : public TdocElement
{
 public:
 TcoverpageElement():TdocElement("coverpage", true)
 {addAllowParent("title-info"); addAllowParent("src-title-info"); addAllowChild("image");};
};
//--------------------------------------------------------------------------
class Tlanguage : public TdocElement
{
Q_OBJECT
// public:
 private:
//typedef struct {QString sname, fname;} Tlang;
class Tlang{public:QString name2, /*name3,*/ fname; Tlang(const QString p1, /*const QString p2,*/ const QString p3): name2(p1),/* name3(p2),*/ fname(p3){};};
// private:
  QList<Tlang> lang; 
  QComboBox * cbo;
 private slots:
  void onCurrentIndexChanged(int index);
 public:
 Tlanguage(const QString name, const bool only):TdocElement(name, only)
// TlangElement():TdocElement("lang", true)
 {
// qDebug() << "Tlanguage" ; 
  addAllowParent("title-info");
  lang.push_back(Tlang("AA", "Afar"));
  lang.push_back(Tlang("AB", "Abkhazian"));
  lang.push_back(Tlang("AF", "Afrikaans"));
  lang.push_back(Tlang("AM", "Amharic"));
  lang.push_back(Tlang("AR", "Arabic"));
  lang.push_back(Tlang("AS", "Assamese"));
  lang.push_back(Tlang("AY", "Aymara"));
  lang.push_back(Tlang("AZ", "Azerbaijani"));
  lang.push_back(Tlang("BA", "Bashkir"));
  lang.push_back(Tlang("BE", "Byelorussian"));
  lang.push_back(Tlang("BG", "Bulgarian"));
  lang.push_back(Tlang("BH", "Bihari"));
  lang.push_back(Tlang("BI", "Bislama"));
  lang.push_back(Tlang("BN", "Bengali" "Bangla"));
  lang.push_back(Tlang("BO", "Tibetan"));
  lang.push_back(Tlang("BR", "Breton"));
  lang.push_back(Tlang("CA", "Catalan"));
  lang.push_back(Tlang("CO", "Corsican"));
  lang.push_back(Tlang("CS", "Czech"));
  lang.push_back(Tlang("CY", "Welsh"));
  lang.push_back(Tlang("DA", "Danish"));
  lang.push_back(Tlang("DE", "German"));
  lang.push_back(Tlang("DZ", "Bhutani"));
  lang.push_back(Tlang("EL", "Greek"));
  lang.push_back(Tlang("EN", "English/American"));
  lang.push_back(Tlang("EO", "Esperanto"));
  lang.push_back(Tlang("ES", "Spanish"));
  lang.push_back(Tlang("ET", "Estonian"));
  lang.push_back(Tlang("EU", "Basque"));
  lang.push_back(Tlang("FA", "Persian"));
  lang.push_back(Tlang("FI", "Finnish"));
  lang.push_back(Tlang("FJ", "Fiji"));
  lang.push_back(Tlang("FO", "Faeroese"));
  lang.push_back(Tlang("FR", "French"));
  lang.push_back(Tlang("FY", "Frisian"));
  lang.push_back(Tlang("GA", "Irish"));
  lang.push_back(Tlang("GD", "Gaelic" "Scots Gaelic"));
  lang.push_back(Tlang("GL", "Galician"));
  lang.push_back(Tlang("GN", "Guarani"));
  lang.push_back(Tlang("GU", "Gujarati"));
  lang.push_back(Tlang("HA", "Hausa"));
  lang.push_back(Tlang("HI", "Hindi"));
  lang.push_back(Tlang("HR", "Croatian"));
  lang.push_back(Tlang("HU", "Hungarian"));
  lang.push_back(Tlang("HY", "Armenian"));
  lang.push_back(Tlang("IA", "Interlingua"));
  lang.push_back(Tlang("IE", "Interlingue"));
  lang.push_back(Tlang("IK", "Inupiak"));
  lang.push_back(Tlang("IN", "Indonesian"));
  lang.push_back(Tlang("IS", "Icelandic"));
  lang.push_back(Tlang("IT", "Italian"));
  lang.push_back(Tlang("IW", "Hebrew"));
  lang.push_back(Tlang("JA", "Japanese"));
  lang.push_back(Tlang("JI", "Yiddish"));
  lang.push_back(Tlang("JW", "Javanese"));
  lang.push_back(Tlang("KA", "Georgian"));
  lang.push_back(Tlang("KK", "Kazakh"));
  lang.push_back(Tlang("KL", "Greenlandic"));
  lang.push_back(Tlang("KM", "Cambodian"));
  lang.push_back(Tlang("KN", "Kannada"));
  lang.push_back(Tlang("KO", "Korean"));
  lang.push_back(Tlang("KS", "Kashmiri"));
  lang.push_back(Tlang("KU", "Kurdish"));
  lang.push_back(Tlang("KY", "Kirghiz"));
  lang.push_back(Tlang("LA", "Latin"));
  lang.push_back(Tlang("LN", "Lingala"));
  lang.push_back(Tlang("LO", "Laothian"));
  lang.push_back(Tlang("LT", "Lithuanian"));
  lang.push_back(Tlang("LV", "Latvian" "Lettish"));
  lang.push_back(Tlang("MG", "Malagasy"));
  lang.push_back(Tlang("MI", "Maori"));
  lang.push_back(Tlang("MK", "Macedonian"));
  lang.push_back(Tlang("ML", "Malayalam"));
  lang.push_back(Tlang("MN", "Mongolian"));
  lang.push_back(Tlang("MO", "Moldavian"));
  lang.push_back(Tlang("MR", "Marathi"));
  lang.push_back(Tlang("MS", "Malay"));
  lang.push_back(Tlang("MT", "Maltese"));
  lang.push_back(Tlang("MY", "Burmese"));
  lang.push_back(Tlang("NA", "Nauru"));
  lang.push_back(Tlang("NE", "Nepali"));
  lang.push_back(Tlang("NL", "Dutch"));
  lang.push_back(Tlang("NO", "Norwegian"));
  lang.push_back(Tlang("OC", "Occitan"));
  lang.push_back(Tlang("OM", "Oromo" "Afan"));
  lang.push_back(Tlang("OR", "Oriya"));
  lang.push_back(Tlang("PA", "Punjabi"));
  lang.push_back(Tlang("PL", "Polish"));
  lang.push_back(Tlang("PS", "Pashto" "Pushto"));
  lang.push_back(Tlang("PT", "Portuguese"));
  lang.push_back(Tlang("QU", "Quechua"));
  lang.push_back(Tlang("RM", "Rhaeto-Romance"));
  lang.push_back(Tlang("RN", "Kirundi"));
  lang.push_back(Tlang("RO", "Romanian"));
  lang.push_back(Tlang("RU", "Russian"));
  lang.push_back(Tlang("RW", "Kinyarwanda"));
  lang.push_back(Tlang("SA", "Sanskrit"));
  lang.push_back(Tlang("SD", "Sindhi"));
  lang.push_back(Tlang("SG", "Sangro"));
  lang.push_back(Tlang("SH", "Serbo-Croatian"));
  lang.push_back(Tlang("SI", "Singhalese"));
  lang.push_back(Tlang("SK", "Slovak"));
  lang.push_back(Tlang("SL", "Slovenian"));
  lang.push_back(Tlang("SM", "Samoan"));
  lang.push_back(Tlang("SN", "Shona"));
  lang.push_back(Tlang("SO", "Somali"));
  lang.push_back(Tlang("SQ", "Albanian"));
  lang.push_back(Tlang("SR", "Serbian"));
  lang.push_back(Tlang("SS", "Siswati"));
  lang.push_back(Tlang("ST", "Sesotho"));
  lang.push_back(Tlang("SU", "Sudanese"));
  lang.push_back(Tlang("SV", "Swedish"));
  lang.push_back(Tlang("SW", "Swahili"));
  lang.push_back(Tlang("TA", "Tamil"));
  lang.push_back(Tlang("TE", "Tegulu"));
  lang.push_back(Tlang("TG", "Tajik"));
  lang.push_back(Tlang("TH", "Thai"));
  lang.push_back(Tlang("TI", "Tigrinya"));
  lang.push_back(Tlang("TK", "Turkmen"));
  lang.push_back(Tlang("TL", "Tagalog"));
  lang.push_back(Tlang("TN", "Setswana"));
  lang.push_back(Tlang("TO", "Tonga"));
  lang.push_back(Tlang("TR", "Turkish"));
  lang.push_back(Tlang("TS", "Tsonga"));
  lang.push_back(Tlang("TT", "Tatar"));
  lang.push_back(Tlang("TW", "Twi"));
  lang.push_back(Tlang("UK", "Ukrainian"));
  lang.push_back(Tlang("UR", "Urdu"));
  lang.push_back(Tlang("UZ", "Uzbek"));
  lang.push_back(Tlang("VI", "Vietnamese"));
  lang.push_back(Tlang("VO", "Volapuk"));
  lang.push_back(Tlang("WO", "Wolof"));
  lang.push_back(Tlang("XH", "Xhosa"));
  lang.push_back(Tlang("YO", "Yoruba"));
  lang.push_back(Tlang("ZH", "Chinese"));
  lang.push_back(Tlang("ZU", "Zulu"));
 };

 virtual QWidget * getWidget(QWidget * parent);
};
//--------------------------------------------------------------------------
class TlangElement : public Tlanguage
{
 public:
 TlangElement():Tlanguage("lang", true){};
};
//--------------------------------------------------------------------------
class TsrcLangElement : public Tlanguage
{
 public:
 TsrcLangElement():Tlanguage("src-lang", true){};
};
//--------------------------------------------------------------------------
class TtranslatorElement : public TpersonElement
{
 public:
 TtranslatorElement():TpersonElement("translator", false){};
};
//--------------------------------------------------------------------------
class TsequenceElement : public TdocElement
{
 public:
 TsequenceElement():TdocElement("sequence", false){addAllowParent("title-info");};
};
//--------------------------------------------------------------------------
class TdocumentInfoElement : public TdocElement
{
 public:
 TdocumentInfoElement():TdocElement("document-info", false)
 {addAllowParent("title-info"); addAllowParent("description");
  addAllowChild("author"); addAllowChild("program-used"); addAllowChild("date"); addAllowChild("src-url"); addAllowChild("src-ocr"); addAllowChild("id"); addAllowChild("version"); addAllowChild("history");};
};
//--------------------------------------------------------------------------
class TsrcUrlElement : public TdocElement
{
 public:
 TsrcUrlElement():TdocElement("src-url", false){/*addAllowParent("title-info");*/};
};
//--------------------------------------------------------------------------
class TsrcOcrElement : public TdocElement
{
 public:
 TsrcOcrElement():TdocElement("src-ocr", true){/*addAllowParent("title-info");*/};
};
//--------------------------------------------------------------------------
class TidElement : public TdocElement
{
 public:
 TidElement():TdocElement("id", true){/*addAllowParent("title-info");*/};
};
//--------------------------------------------------------------------------
class TversionElement : public TdocElement
{
 public:
 TversionElement():TdocElement("version", true){/*addAllowParent("title-info");*/};
};
//--------------------------------------------------------------------------
class ThistoryElement : public TdocElement
{
 public:
 ThistoryElement():TdocElement("history", true)
 {/*addAllowParent("title-info");*/
  addAllowChild("p"); addAllowChild("poem"); addAllowChild("cite"); addAllowChild("subtitle"); addAllowChild("table"); addAllowChild("empty-line"); };
};
//--------------------------------------------------------------------------
class TbookNameElement : public TdocElement
{
 public:
 TbookNameElement():TdocElement("book-name", true){addAllowParent("publish-info");};
};
//--------------------------------------------------------------------------
class TpublisherElement : public TdocElement
{
 public:
 TpublisherElement():TdocElement("publisher", true){addAllowParent("publish-info");};
};
//--------------------------------------------------------------------------
class TcityElement : public TdocElement
{
 public:
 TcityElement():TdocElement("city", true){addAllowParent("publish-info");};
};
//--------------------------------------------------------------------------
class TyearElement : public TdocElement
{
 public:
 TyearElement():TdocElement("year", true){addAllowParent("publish-info");};
};
//--------------------------------------------------------------------------
class TbodyElement : public TdocElement
{
 public:
 TbodyElement():TdocElement("body", false)
 {addAllowChild("image"); addAllowChild("title"); addAllowChild("epigraph"); addAllowChild("section");};
 virtual QWidget * getWidget(QWidget * parent) {return (new QWidget(parent));};
};
//--------------------------------------------------------------------------
class TsectionElement : public TformatTextElement
{
 public:
 TsectionElement():TformatTextElement("section", false)
 {addAllowParent("body"); addAllowParent("section");
  addAllowChild("image"); addAllowChild("title"); addAllowChild("epigraph"); addAllowChild("annotation");
  addAllowChild("section");addAllowChild("p"); addAllowChild("poem"); addAllowChild("cite");
  addAllowChild("subtitle"); addAllowChild("table"); addAllowChild("empty-line");};
};
//--------------------------------------------------------------------------
class TblockTextElement : public TdocElement
{
 Q_OBJECT
 public:
 TblockTextElement(const QString name, const bool only):TdocElement(name, only)
 {text = true; addAllowParent("title");addAllowParent("cite");addAllowParent("epigraph");
               addAllowParent("annotation"); addAllowParent("section");};
// virtual QString getValue() const {return(value);};
// virtual bool initElement(QDomElement * domElement);
 virtual QWidget * getWidget(QWidget * parent);

 QTextCharFormat format;            // текущее форматирование вывода

 virtual bool isBlockText() {return (true);};

 virtual void insertInto(QTextEdit * e);

 public slots:
  virtual void onTextChanged();
};
//--------------------------------------------------------------------------
class TpElement : public TblockTextElement
{
 Q_OBJECT
 public:
 TpElement():TblockTextElement("p", false)
 {
  addAllowParent("history");
  addAllowChild("text"); addAllowChild("strong"); addAllowChild("emphasis"); addAllowChild("style");
  addAllowChild("a"); addAllowChild("strikethrough"); addAllowChild("sub"); addAllowChild("sup");
  addAllowChild("code"); addAllowChild("image");
 };

 virtual QMenu * getItemMenu();
 public slots:
 void convert_to_section();
// void insertInto(QTextEdit * e){e->insertPlainText(value); e->textCursor().insertBlock();};
};
//--------------------------------------------------------------------------
class TpoemElement : public TblockTextElement 
{
 public:
 TpoemElement():TblockTextElement("poem", false)
 {
  addAllowChild("stanza"); addAllowChild("text-author"); addAllowChild("date");
 };
};
//--------------------------------------------------------------------------
class TstanzaElement : public TdocElement
{
 public:
 TstanzaElement():TdocElement("stanza", false)
 {
  addAllowParent("poem");
  addAllowChild("title"); addAllowChild("subtitle"); addAllowChild("v");
 };
};
//--------------------------------------------------------------------------
class TvElement : public TdocElement
{
 public:
 TvElement():TdocElement("v", false)
 {
  addAllowParent("stanza");
  addAllowChild("strong"); addAllowChild("emphasis"); addAllowChild("style");
  addAllowChild("a"); addAllowChild("strikethrough"); addAllowChild("sub");
  addAllowChild("sup"); addAllowChild("code"); addAllowChild("image");
 };
};
//--------------------------------------------------------------------------
class TtableElement : public TblockTextElement 
{
 public:
 TtableElement():TblockTextElement("table", false) {text = true;};
};
//--------------------------------------------------------------------------
class TemptyLineElement : public TblockTextElement
{
 public:
 TemptyLineElement():TblockTextElement("empty-line", false)
 {};
 void insertInto(QTextEdit * e){e->textCursor().insertBlock();};
};
//--------------------------------------------------------------------------
class TfontStyleElement : public TdocElement
{
 public:
 TfontStyleElement(const QString name, const bool only):TdocElement(name, only)
 {
  text = true; addAllowParent("p"); addAllowParent("v");
               addAllowParent("subtitle"); addAllowParent("text-author");
 };
 virtual void getFormat(QTextCharFormat * f) const = 0;

 virtual bool isFontStyle() {return (true);};
};
//--------------------------------------------------------------------------
class TemphasisElement : public TfontStyleElement // курсив
{
 public:
 TemphasisElement():TfontStyleElement("emphasis", false){;};
 virtual void getFormat(QTextCharFormat * f) const {f->setFontItalic(true);};
};
//--------------------------------------------------------------------------
class TstrongElement : public TfontStyleElement // жирный
{
 public:
 TstrongElement():TfontStyleElement("strong", false){;};
 virtual void getFormat(QTextCharFormat * f) const {f->setFontWeight(QFont::Bold);};
};
//--------------------------------------------------------------------------
class TsubElement : public TfontStyleElement // Верхний индекс
{
 public:
 TsubElement():TfontStyleElement("sub", false){;};
 virtual void getFormat(QTextCharFormat * f) const {qDebug() << "not TsubElement";};
};
//--------------------------------------------------------------------------
class TsupElement : public TfontStyleElement // Нижний индекс
{
 public:
 TsupElement():TfontStyleElement("sup", false){;};
 virtual void getFormat(QTextCharFormat * f) const {qDebug() << "not TsupElement";};
};
//--------------------------------------------------------------------------
class TcodeElement : public TfontStyleElement // программный код
{
 public:
 TcodeElement():TfontStyleElement("code", false){;};
 virtual void getFormat(QTextCharFormat * f) const {f->setFontFixedPitch(true);};
};
//--------------------------------------------------------------------------
class TtitleElement : public TformatTextElement // заголовок
{
 public:
 TtitleElement():TformatTextElement("title", true)
 {text = true; addAllowParent("body"); addAllowParent("section");addAllowParent("poem"); addAllowParent("stanza");
               addAllowChild("p"); addAllowParent("empty-line");};
};
//--------------------------------------------------------------------------
class TsubtitleElement : public TdocElement // подзаголовок
{
 public:
 TsubtitleElement():TdocElement("subtitle", true)
 {text = false; addAllowParent("body"); addAllowParent("section");addAllowParent("cite");addAllowParent("stanza");
 };
};
//--------------------------------------------------------------------------
class TepigraphElement : public TformatTextElement // эпиграф
{
 public:
 TepigraphElement():TformatTextElement("epigraph", false)
 {text = false;
  addAllowParent("body"); addAllowParent("section"); addAllowParent("poem");
  addAllowChild("p"); addAllowChild("poem");
  addAllowChild("cite"); addAllowChild("empty-line"); addAllowChild("text-author");
 };
};
//--------------------------------------------------------------------------
class TciteElement : public TblockTextElement  // цитата
{
 public:
 TciteElement():TblockTextElement("cite", true)
 {
  addAllowChild("p"); addAllowChild("poem"); addAllowChild("empty-line");  addAllowChild("subtitle"); 
  addAllowChild("table"); addAllowChild("text-author");
 };
};
//--------------------------------------------------------------------------
class TimageElement : public TdocElement // картинка
{
 protected:
 // QByteArray value;
 public:
 TimageElement():TdocElement("image", true)
 {text = false;
  addAllowParent("body"); addAllowParent("section"); addAllowParent("p"); addAllowParent("coverpage");
  addAllowParent("v"); addAllowParent("subtitle"); addAllowParent("th"); addAllowParent("td");
  addAllowParent("text-author");
  addAllowChild("none-allow-child");
 };
 virtual QWidget * getWidget(QWidget * parent);
};
//--------------------------------------------------------------------------
class TbinaryElement : public TdocElement // картинка
{
//Q_OBJECT
 protected:
  QByteArray value;
 public:
 TbinaryElement():TdocElement("binary", true) {text = false; };
 TbinaryElement(const QString fileName);
 TbinaryElement(const QByteArray & other, const QString fileName);

 virtual QWidget * getWidget(QWidget * parent);
 virtual QString   getItemName(); 
 virtual QByteArray getData() {return(value);};
 virtual bool initElement(QDomElement * domElement); // импорт данных из QDomElement *
// bool saveElement(QDomDocument * doc, QDomElement * parent)
 virtual QString getValue() const;
//public slots:
 virtual void setValue(const QString & p);
};
//--------------------------------------------------------------------------
class TtextElement : public TdocElement // текст
{
//Q_OBJECT
 public:
 TtextElement():TdocElement("text", true)
 {text = false;};
 virtual QStandardItemDoc * getItem() {return (NULL);};  // не нужно отображать в дереве
// virtual bool saveElement(QDomDocument * doc, QDomElement * domElement); // экспорт данных в QDomElement *
};
//--------------------------------------------------------------------------
TdocElement * xml2doc(const QDomNode& node, TdocElement * root);
TdocElement * createElement(const QString p);
void document2doc(QTextDocument * document, TdocElement * root);
//--------------------------------------------------------------------------
#endif
