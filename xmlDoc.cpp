//--------------------------------------------------------------------------
//#include <QIODevice>
//#include <QFile>
#include <QFileDialog>
#include "xmlDoc.h"
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TxmlDoc::TxmlDoc(QWidget * pwgt) : QWidget(pwgt), selModel(&treeModel)
{
 lBuffer.clear();
 setupUi(this);

// connect(lineEdit, SIGNAL(returnPressed()), SLOT(onReturnPressed()));
}
//--------------------------------------------------------------------------
TxmlDoc::~TxmlDoc()
{
 if (!fileName.isEmpty()) delete (doc);  
}
//--------------------------------------------------------------------------
void TxmlDoc::setupUi(QWidget * p)
{
 Ui::XmlDoc::setupUi(p);

 // заменяем стандартный виджет на своего наследника
 delete(treeView);
 treeView = new TxmlTreeView(splitter);  treeView->setObjectName(QString::fromUtf8("treeView"));
 //treeView->setSelectionMode(QAbstractItemView::ContiguousSelection);//(QAbstractItemView::MultiSelection);
 treeView->setSelectionMode(QAbstractItemView::ExtendedSelection);
 splitter->insertWidget(0, treeView); 

 setupMainMenu();

 bOpen->setDefaultAction(file_open);

 connect(&selModel, SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
                    SLOT(onSelectionChanged(const QItemSelection &, const QItemSelection &)));
 
 connect(treeView, SIGNAL(mousePressButtonRight(QPoint)), SLOT(onMousePressButtonRight(QPoint)));
 connect(treeView, SIGNAL(keyPress(QKeyEvent *)), SLOT(onTreeKeyPressEvent(QKeyEvent *)));

// file_open->setEnabled(false);
}
//--------------------------------------------------------------------------
void TxmlDoc::setupMainMenu()
{
 QAction * a;

 mb = new QMenuBar(this);
 mb->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);

 verticalLayout->insertWidget(0, mb/*, 1, Qt::AlignTop | Qt::AlignLeft*/);

 mfile = mb->addMenu("File");

 a = new QAction("New", this); a->setObjectName("newfile"); mfile->addAction(a);
 connect(a, SIGNAL(triggered()), SLOT(onNewFilePressed()));

 file_open = new QAction("Open", this); mfile->addAction(file_open);
 connect(file_open, SIGNAL(triggered()), this, SLOT(onOpenPressed()));

 file_save  = new QAction("Save", this); mfile->addAction(file_save);
 connect(file_save, SIGNAL(triggered()), this, SLOT(onSavePressed()));

 file_save_as  = new QAction("Save as", this); mfile->addAction(file_save_as);
 connect(file_save_as, SIGNAL(triggered()), this, SLOT(onSaveAsPressed()));

 mfile->addSeparator();

 mfile->addAction(QIcon(), "Exit", this, SLOT(close()));

 QMenu * medit = mb->addMenu("Edit");

 a = new QAction("copy", this); a->setData("copy"); a->setObjectName("copy"); medit->addAction(a);
 connect(a, SIGNAL(triggered()), SLOT(onItemCopy()));
 a = new QAction("paste",this); a->setData("paste");a->setObjectName("paste");medit->addAction(a);
 connect(a, SIGNAL(triggered()), SLOT(onItemPaste()));
// TODO : do paste
 a->setEnabled(false); 
 a = new QAction("move",this);  a->setData("move"); a->setObjectName("move");medit->addAction(a);
 connect(a, SIGNAL(triggered()), SLOT(onItemMove()));

 medit->addSeparator();

// a = new QAction("create binary",this);  a->setData("createbinary"); medit->addAction(a);
// connect(a, SIGNAL(triggered()), SLOT(onCreateBinary()));
 a = new QAction("create image from file",this);  a->setData("createimagefromfile"); medit->addAction(a);
 connect(a, SIGNAL(triggered()), SLOT(onCreateImageFromFile()));
 a = new QAction("create image from clipboard",this);  a->setData("createimagefromclipboard"); medit->addAction(a);
 connect(a, SIGNAL(triggered()), SLOT(onCreateImageFromClipboard()));

 a = new QAction("import text file",this);  a->setData("importtextfile"); medit->addAction(a);
 connect(a, SIGNAL(triggered()), SLOT(onImportTextFile()));
 a = new QAction("import text from clipboard",this);  a->setData("importtextfromclipboard"); medit->addAction(a);
 connect(a, SIGNAL(triggered()), SLOT(onImportTextFromClipboard()));

 medit->addSeparator();

 a = new QAction("refresh tree",this);  a->setData("refreshtree"); medit->addAction(a);
 connect(a, SIGNAL(triggered()), SLOT(onTreeRefresh()));

// file_save_as
// file_exit
}
//--------------------------------------------------------------------------
bool TxmlDoc::open(const QString p)
{
 bool ret(false);
 QFile xFile;
 
 if (!p.isEmpty()) fileName = p;

 if (!fileName.isEmpty()) setWindowTitle("xmlDoc: " + fileName);
 else {setWindowTitle("xmlDoc: "); return(ret);}

 treeView->setModel(new QStandardItemModel());
 treeModel.clear();

 xFile.setFileName(fileName);
 if (xFile.open(QIODevice::ReadOnly))
 {
  if (domDoc.setContent(&xFile))
  {
   doc = xml2doc(domDoc.documentElement(), NULL); 
   doc->item = dynamic_cast<QStandardItemDoc *>(treeModel.invisibleRootItem());
   onTreeAppendItem(doc, treeModel.invisibleRootItem());

   treeView->setModel(&treeModel);
   treeView->setSelectionModel(&selModel);
  }
 }
 else
 {
  return(ret);
 } 
 
 xFile.close();

 return(ret);
}
//--------------------------------------------------------------------------
bool TxmlDoc::save(const QString p)
{
 bool ret(false);
 QDomElement root;
 QDomDocument domDocument;
 QFile file;
 QList<TdocElement *>::iterator itr;
 QMap<QString, QString>::iterator itra; 

 fileName = p;
 if (!fileName.isEmpty()) setWindowTitle("xmlDoc: " + fileName);
 else {setWindowTitle("xmlDoc: "); return(false);}

 QDomNode xml_node = domDocument.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"windows-1251\"");
 root = domDocument.createElement(doc->getName());
 domDocument.appendChild(root);
 domDocument.insertBefore(xml_node, domDocument.firstChild());

 for(itra = doc->attribute.begin(); itra != doc->attribute.end(); ++itra)
 {
  root.setAttribute(itra.key(), itra.value());
 }

 for(itr = doc->child.begin(); itr != doc->child.end(); ++itr)
 {
  (*itr)->saveElement(&domDocument, &root);
 }

 file.setFileName(fileName);
 if (file.open(QIODevice::WriteOnly))
 {
  QTextStream out(&file);
  out.setCodec("Windows-1251");
  //out << domDocument.toString();
  domDocument.save(out, 0, QDomNode::EncodingFromTextStream);
  file.close();
  ret = true;
 }

 return(ret);
}
//--------------------------------------------------------------------------
void TxmlDoc::onNewFilePressed()
{
 open(":/emptyFile");

 setWindowTitle("xmlDoc: * New *");
}
//--------------------------------------------------------------------------
void TxmlDoc::onOpenPressed()
{
 QString fileName = QFileDialog::getOpenFileName(this, 
                                         tr("Open file"), ".",
                                         tr("Xml Files (*.fb2 *.epub *.xml);;All Files(*.*)"));
 open(fileName);
}
//--------------------------------------------------------------------------
void TxmlDoc::onSavePressed()
{
 save(fileName);
}
//--------------------------------------------------------------------------
void TxmlDoc::onSaveAsPressed()
{
 QString fileName = QFileDialog::getSaveFileName(this,
                                         tr("Save file"), ".",
                                         tr("Xml Files (*.fb2 *.epub *.xml);;All Files(*.*)"));
 if (!fileName.isEmpty())
 {save(fileName);}
}
//--------------------------------------------------------------------------
bool iListSort(const QModelIndex &i1, const QModelIndex &i2)
{
 if (i1.column() < i2.column()) return (true);
 if (i1.row() < i2.row()) return (true);
 return (false);
}
//--------------------------------------------------------------------------
void TxmlDoc::onItemCopy()
{
 QModelIndexList::iterator itr;
 QStandardItemModel * model = dynamic_cast<QStandardItemModel *>(treeView->model());
 QModelIndexList      iList = treeView->selectionModel()->selectedIndexes();

 qSort(iList.begin(), iList.end(), iListSort);

 lBuffer.clear();
 for(itr = iList.begin(); itr != iList.end(); ++ itr)
 {
  QStandardItemDoc * iDoc = dynamic_cast<QStandardItemDoc *>(model->itemFromIndex(*itr));
//  qDebug() << iDoc->doc->getName() << itr->row() << " " << itr->column();
  lBuffer.push_back(iDoc->doc);
 }

 if (lBuffer.empty()) {lBuffer.push_back(currentItem->doc);}
}
//--------------------------------------------------------------------------
void TxmlDoc::onItemPaste()
{

}
//--------------------------------------------------------------------------
void TxmlDoc::onItemMove()
{
 QList<TdocElement *>::iterator itr;

 for(itr = lBuffer.begin(); itr != lBuffer.end(); ++itr)
 {
  (*itr)->parent->removeChild(*itr);
  currentItem->doc->addChild(*itr);
  currentItem->appendRow((*itr)->getItem());
 }
}
//--------------------------------------------------------------------------
/*void TxmlDoc::onCreateBinary()
{
 TbinaryElement * bin;
 QString fileName = QFileDialog::getOpenFileName(this,
                                         tr("Open file"), ".",
                                         tr("Image Files (*.jpg *.png);;All Files(*.*)"));
 if(fileName.isEmpty()) return;

 bin = new TbinaryElement(fileName);
 doc->addChild(bin);
 treeModel.invisibleRootItem()->appendRow(bin->getItem());
}*/
//--------------------------------------------------------------------------
void TxmlDoc::onCreateImageFromFile()
{
 TbinaryElement * bin;
 TimageElement  * img; 
// QString fileName = QFileDialog::getOpenFileName(this,
//                                         tr("Open file"), ".",
//                                         tr("Image Files (*.jpg *.png);;All Files(*.*)"));
// if(fileName.isEmpty()) return;

 QStringList::iterator itr;
 QStringList lFiles = QFileDialog::getOpenFileNames(this,
                                         tr("Open one or more files"), ".",
                                         tr("Image Files (*.jpg *.png);;All Files(*.*)"));

 for(itr = lFiles.begin(); itr != lFiles.end(); ++itr)
 {
  QString fileName (*itr);
  bin = new TbinaryElement(fileName);
  doc->addChild(bin);
  treeModel.invisibleRootItem()->appendRow(bin->getItem());

  img = new TimageElement();
  img->attribute.insert("l:href", "#" + bin->attribute.value("id"));

  currentItem->doc->addChild(img);
  currentItem->appendRow(img->getItem()); 
 }
}
//--------------------------------------------------------------------------
void TxmlDoc::onCreateImageFromClipboard()
{
 TbinaryElement * bin;
 TimageElement  * img;
 QString          fileName(QString("%1.jpg").arg(QDateTime::currentMSecsSinceEpoch(), 8, 16));
 QByteArray     ba;
 QBuffer        buffer(&ba);
 QClipboard * cb = QApplication::clipboard();

 //QMimeData * mime ( cb->mimeData() );
 if (!cb->mimeData()->hasImage())
 {QMessageBox::information(this, "Warning", "There is no image in Clipboard."); return;}

 QImage image = qvariant_cast<QImage>(cb->mimeData()->imageData());

 buffer.open(QIODevice::WriteOnly);
 image.save(&buffer, "JPG"); // writes image into ba in PNG format
 buffer.close();

 bin = new TbinaryElement(ba, fileName);
 doc->addChild(bin);
 treeModel.invisibleRootItem()->appendRow(bin->getItem());

 img = new TimageElement();
 img->attribute.insert("l:href", "#" + bin->attribute.value("id"));

 currentItem->doc->addChild(img);
 currentItem->appendRow(img->getItem());
}
//--------------------------------------------------------------------------
void TxmlDoc::onImportTextFile()
{
 QFile file;
 char buffer[64]; qint64 l;
 QString s;
// QTextCodec * codec = QTextCodec::codecForName("UTF-8");
 QTextCodec * codec = QTextCodec::codecForName("Windows-1251");
 QString fileName = QFileDialog::getOpenFileName(this,
                                         tr("Open file"), ".",
                                         tr("Text Files (*.txt);;All Files(*.*)"));
 if(fileName.isEmpty()) return;

 file.setFileName(fileName);

 if (!file.exists()) return;

 if (!file.open(QIODevice::ReadOnly)) return;
// value = file.readAll();

 while (!file.atEnd())
 {
  TdocElement * p;
  while((l = file.readLine(buffer, sizeof(buffer))) > 0)
  {
   s += codec->toUnicode(buffer, l);
   if (l <= sizeof(buffer) - 1 && buffer[l - 1] == '\n') break;
  }
  
  s = s.trimmed();
  if (!s.isEmpty())
  {
   p = new TpElement();
   p->setValue(s);
  }
  else
  {
   p = new TemptyLineElement();
  }
  currentItem->doc->addChild(p);
  s.clear();
 }

 file.close();

 onTreeRefresh(); 
}
//--------------------------------------------------------------------------
void TxmlDoc::onImportTextFromClipboard()
{
 QClipboard * cb = QApplication::clipboard();
 QString str = cb->text();
 QStringList lst; QStringList::iterator itr;

 if (str.isNull()) return;

 lst = str.split("\n");

 for(itr = lst.begin(); itr != lst.end(); ++itr)
 {
  TdocElement * p;
  if (!itr->isEmpty())
  {
   p = new TpElement();
   p->setValue(*itr);
  }
  else
  {
   p = new TemptyLineElement();
  }
  currentItem->doc->addChild(p);
 }
 onTreeRefresh();
}
//--------------------------------------------------------------------------
//QStandardItemDoc * TxmlDoc::getCurrentItem(void) const
//{
//}
//--------------------------------------------------------------------------
void TxmlDoc::onSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
 QWidget * w;
 TdocElement * e; 
 QList<QModelIndex> ilist = selected.indexes();
 QList<QModelIndex>::iterator itr;
 itr = ilist.begin();
 if (itr != ilist.end())
 {
  QModelIndex index = *itr;
  QStandardItemDoc * item = static_cast<QStandardItemDoc *>(treeModel.itemFromIndex(index));

  currentItem = item;

  while(QLayoutItem * i = vBox->takeAt(0)) {vBox->removeItem(i); delete(i->widget());}

  e = item->doc;
  w = e->getWidget(NULL);
  if (w == NULL) return;
  vBox->addWidget(w);

  if (e->isFormatText())
  {connect(e, SIGNAL(onTreeSetIndex(const QModelIndex&)), SLOT(onTreeSetIndex(const QModelIndex&)));}
//  vBox->addItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));
 }

}
//--------------------------------------------------------------------------
void TxmlDoc::onMousePressButtonRight(QPoint p)
{
 currentItem->doc->onItemMousePressButtonRight(p);
}
//--------------------------------------------------------------------------
void TxmlDoc::onTreeKeyPressEvent(QKeyEvent * event)
{
 if (event->key() != Qt::Key_Delete) return;

 QList<TdocElement *> lElement;
 QList<TdocElement *>::iterator litr;
 QModelIndexList::iterator itr;
 QStandardItemModel * model = dynamic_cast<QStandardItemModel *>(treeView->model());
 QModelIndexList      iList = treeView->selectionModel()->selectedIndexes();

 if (!iList.empty())
 {
  for(itr = iList.begin(); itr != iList.end(); ++itr)
  {
   QStandardItemDoc * iDoc = dynamic_cast<QStandardItemDoc *>(model->itemFromIndex(*itr));
   lElement.push_back(iDoc->doc);
  }
  for(litr = lElement.begin(); litr != lElement.end(); ++litr) 
  {(*litr)->onDelete();}
 }
 else
 {
  currentItem->doc->onDelete(); 
 }

}
//--------------------------------------------------------------------------
//void TxmlDoc::onMenuPressed()
//{
// qDebug() << "onMenuPressed " << sender()->objectName();
 //currentItem->doc->onMenuPressed(sender()->objectName());
// if (currentItem->doc->onMenuPressed(sender()->objectName()))
// {
//  onTreeRefresh();
// }
//}
//--------------------------------------------------------------------------
void TxmlDoc::onTreeRefresh()
{
// QModelIndex index = currentItem->index();

 treeModel.clear();
 onTreeAppendItem(doc, treeModel.invisibleRootItem());
 treeView->setModel(&treeModel);
 treeView->setSelectionModel(&selModel);
// 
// treeView->setCurrentIndex(index);
}
//--------------------------------------------------------------------------
void TxmlDoc::onTreeSetIndex(const QModelIndex& index)
{
 qDebug() << __PRETTY_FUNCTION__;
 if (index.isValid())
  {treeView->setCurrentIndex(index);}
 //else
 //{qDebug() << "invalid index";}
}
//--------------------------------------------------------------------------
//void TxmlDoc::onTreeUpItem(QStandardItem * item)
//{
// QStandardItem * parent = item->parent();
// int i = item->row();
//
//qDebug() << "TxmlDoc::onTreeUpItem 0 i=" << i << " parent=" << (int)parent;
//
// if (i == -1 || parent == 0) return;
//
// parent->insertRow(i - 1, parent->takeChild(i));
// parent->removeRow(i + 1);
//
//qDebug() << "TxmlDoc::onTreeUpItem 10";
//
//}
//--------------------------------------------------------------------------
//void TxmlDoc::onTreeDownItem(QStandardItem * item)
//{
// QStandardItem * parent = item->parent();
// int i = item->row();
//
// if (1 == -1) return;
//
// parent->insertRow(i + 2, parent->takeChild(i));
// parent->removeRow(i);
//}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
/*void TxmlDoc::traverseNode(const QDomNode& node, QStandardItem * item)
{

 if(node.isElement())
 {
  QDomElement domElement = node.toElement();
  if(!domElement.isNull())
  {
    item->setText(domElement.tagName());
   }
 }

   QDomNode domNode = node.firstChild();
   while(!domNode.isNull())
   {
    if (!domNode.isNull() && domNode.isElement() && domNode.toElement().tagName().trimmed().isEmpty())
    { domNode = domNode.nextSibling(); continue;}
   

    QStandardItemXml * child = new QStandardItemXml();
    child->node = domNode;
// connect(child, SIGNAL(activated(const QModelIndex&)), SLOT(onActivatedTree(const QModelIndex &index)));
    item->appendRow(child);
    traverseNode(domNode, child);
    domNode = domNode.nextSibling();
   }  
}*/
//--------------------------------------------------------------------------
void TxmlDoc::onTreeAppendItem(TdocElement * root, QStandardItem * item)
{
 QList<TdocElement *>::iterator itr;

 for(itr = root->child.begin(); itr != root->child.end(); ++itr)
 {
  QStandardItemDoc * child = (*itr)->getItem();
  if (child != NULL)
  {
   item->appendRow(child);
   connect((*itr), SIGNAL(onTreeSetIndex(const QModelIndex&)), SLOT(onTreeSetIndex(const QModelIndex&)) );
 //  connect((*itr), SIGNAL(onTreeUpItem(QStandardItem *)), SLOT(onTreeUpItem(QStandardItem *)) );
 //  connect((*itr), SIGNAL(onTreeDownItem(QStandardItem *)), SLOT(onTreeDownItem(QStandardItem *)) );
  }
 }
}
//--------------------------------------------------------------------------
