//
//
//
//---------------------------------------------------------------------------
#include <QtGui>
//#include <QFile>
#include "xmlDoc.h"
#include "richTextEdit.h"
#include "qxml.h"
//---------------------------------------------------------------------------
#ifdef USE_DEBUG_PROFILE
TProfile pro[DEBUG_PROFILE_SIZE];
#endif
//QApplication * qa;
//---------------------------------------------------------------------------
int main(int argc, char **argv)
{
// QFile f;
 QString fileName;
// read command line
 for(int i = 1; i < argc; i++)
 {
  QString s(argv[i]);
  if (s.left(2) == QString("--"))
  {
   if (s == QString("--help"))
   {
    //print_help();
    return(0);
   }
   i++;
  } // if (s.substr(0,2) == std::string("--"))
  else
  {
   if (s.right(s.length() - s.lastIndexOf(".")  - 1) == "fb2") fileName = s;
   else fileName.clear();
  }
 } // for(int i = 1; i < argc; i++)

 QApplication app(argc, argv);

 TxmlDoc doc;
 if (!fileName.isEmpty()){doc.open(fileName);}
 else {doc.onNewFilePressed();}
 doc.show();

// TrichTextEdit e; e.show();
 
 return app.exec();
}
//---------------------------------------------------------------------------
