//--------------------------------------------------------------------------
#include <QTextCursor>
#include "richTextEdit.h"
#include "docElement.h"
//--------------------------------------------------------------------------
TdocElement::TdocElement(const QString p) : name(p), only(false), text(false) 
{
 value.clear();
 item   = NULL;
 parent = NULL;
}
TdocElement::TdocElement(const QString p, const bool b) : name(p), only(b), text(false)
{
 value.clear();
 item   = NULL;
 parent = NULL;
}
//--------------------------------------------------------------------------
TdocElement::~TdocElement()
{
 clearChild();
}
//--------------------------------------------------------------------------
TdocElement * TdocElement::getChild(const QString name)
{
 QString up(name.toUpper());
 QList<TdocElement *>::iterator itr;
 for(itr = child.begin(); itr != child.end(); ++itr)
 {
  if ((*itr)->getName().toUpper() == up)
  {
   return (*itr);
  }
 }
 return(NULL);
}
//--------------------------------------------------------------------------
TdocElement * TdocElement::getParent(const QString name)
{
 TdocElement * ret = parent;

 if (name.isEmpty()) return(parent);

 while(ret != NULL)
 {
  if (ret->getName() == name) break;
  ret = ret->parent;
 }

 return(ret);
}
//--------------------------------------------------------------------------
bool TdocElement::setParent(TdocElement * p)
{
 parent = p;
 return(true);
}
//--------------------------------------------------------------------------
bool TdocElement::addChild(TdocElement * p)
{
 child.push_back(p);
 p->setParent(this);
 return(true); 
}
//--------------------------------------------------------------------------
bool TdocElement::insertChild(TdocElement * src, TdocElement * dst)
{
// bool ret(false);
 int i;
 QList<TdocElement *>::iterator itr;

 if (dst == NULL)
 {
qDebug() << "TdocElement::insertChild dst == NULL";
  return(addChild(src));
 }

 i = childPosition(dst);
 if (i < 0) {return(false);}
 i += 1;
 child.insert(i, src);

 src->setParent(this);
 return(true);
}
//--------------------------------------------------------------------------
void TdocElement::removeChild(TdocElement * e)  // удалит из списка, объект не удаляется
{
 int i(0);
 QList<TdocElement *>::iterator itr;

//qDebug() << "removeChild 0";

 for(itr = child.begin(); itr != child.end(); ++itr, ++i)
 {
//qDebug() << "removeChild 1";
  if (e == *itr) {
//qDebug() << "removeChild 2";
child.erase(itr);
//qDebug() << "removeChild 3";
 if (item != NULL)
 item->removeRow(i);
// child.erase(itr);// else emit 
//qDebug() << "removeChild 4";
 break;}
 }
//qDebug() << "removeChild 5";
}
void TdocElement::removeChild(const int p)  // удалит из списка, объект не удаляется
{
 if (p >=0 && p < child.size()) 
 {child.removeAt(p); item->removeRow(p);}
}
//--------------------------------------------------------------------------
bool TdocElement::moveChild(TdocElement * src, TdocElement * new_root)
{
 if (src->parent != NULL)
  src->parent->removeChild(src);
 else return(false);
 if (new_root != NULL)
  new_root->addChild(src);
 else return(false);
 return(true);
}
//--------------------------------------------------------------------------
int TdocElement::childPosition(TdocElement * e)
{
 int i(0);
 QList<TdocElement *>::iterator itr;
 for(itr = child.begin(); itr != child.end(); ++itr, ++i)
 {
  if (e == *itr)
  {
qDebug() << e->getName() << " i=" << i;
   return(i);
  }
 }
qDebug() << e->getName() << " i=-1";
 return(-1);
}
//--------------------------------------------------------------------------
void TdocElement::clearChild()
{
 QList<TdocElement *>::iterator itr;
 for(itr = child.begin(); itr != child.end(); ++itr)
 {
  delete(*itr);
 }
 child.clear();
}
//--------------------------------------------------------------------------
void TdocElement::upChild(TdocElement * e)
{
 QList<TdocElement *>::iterator itr;
 int i(0);
// qDebug() << "1";
 for(itr = child.begin(); itr != child.end(); ++itr, ++i)
 {
// qDebug() << "15";
  if (e == *itr && i != 0)
  {
   child.swap(i - 1, i);
//   item->removeRow(i); item->insertRow(i - 1, e->getItem());
   item->insertRow(i - 1, item->takeChild(i));
   item->removeRow(i + 1);
//qDebug() << "emit";
//   emit(onTreeUpItem(item));
   break;
  }
 }
// qDebug() << "2";
}
//--------------------------------------------------------------------------
void TdocElement::downChild(TdocElement * e)
{
 QList<TdocElement *>::iterator itr;
 int i(0);
 for(itr = child.begin(); itr != child.end(); ++itr, ++i)
 {
  if (e == *itr && i < child.size() - 1)
  {
   child.swap(i, i + 1);
   //item->removeRow(i); item->insertRow(i + 1, e->getItem());
   item->insertRow(i + 2, item->takeChild(i));
   item->removeRow(i);
   break;
  }
 }
}
//--------------------------------------------------------------------------
bool TdocElement::checkAllowParent(const QString typeName)// const
{
 QList<QString>::iterator itr;
 for(itr = allowParent.begin(); itr != allowParent.end(); ++itr)
 {
  if (typeName == *itr) {return(true);}
 }
 return(false); 
}
//--------------------------------------------------------------------------
bool TdocElement::checkAllowChild(const QString typeName)// const
{
 QList<QString>::iterator itr;
 for(itr = allowChild.begin(); itr != allowChild.end(); ++itr)
 {
  if (typeName == *itr) {return(true);}
 }
 return(false);
}
//--------------------------------------------------------------------------
void TdocElement::onItemMousePressButtonRight(QPoint p)
{
 QMenu * menu = getItemMenu();
 menu->exec(p);
}
//--------------------------------------------------------------------------
QMenu * TdocElement::getItemMenu()
{
 QList<QString>::iterator itr;
 QAction * a;

 QMenu * menu = new QMenu(NULL);

 a = new QAction("delete", this); a->setData("delete"); a->setObjectName("delete"); menu->addAction(a);
 connect(a, SIGNAL(triggered()), this, SLOT(onDelete()));
 a = new QAction("up",     this); a->setData("up");     a->setObjectName("up");     menu->addAction(a);
 connect(a, SIGNAL(triggered()), this, SLOT(onMenuPressed()));
 a = new QAction("down",   this); a->setData("down");   a->setObjectName("down");   menu->addAction(a);
 connect(a, SIGNAL(triggered()), this, SLOT(onMenuPressed()));
 menu->addSeparator();

// a = new QAction("copy", this); a->setData("copy"); a->setObjectName("copy"); menu->addAction(a);
// connect(a, SIGNAL(triggered()), this, SLOT(onCopy()));
// a = new QAction("paste",this); a->setData("paste");a->setObjectName("paste");menu->addAction(a);
// connect(a, SIGNAL(triggered()), this, SLOT(onPaste()));
// menu->addSeparator();

 for(itr = allowChild.begin(); itr != allowChild.end(); ++itr)
 {
  a = new QAction(*itr, this);
  a->setData(*itr);
  a->setObjectName(*itr);
  connect(a, SIGNAL(triggered()), this, SLOT(onMenuPressed()));
  menu->addAction(a);
 }

 return(menu);
}
//--------------------------------------------------------------------------
QWidget * TdocElement::getWidget(QWidget * parent)
{
 QTextEdit * e = new QTextEdit(parent);
 QTextCursor c;
 /*TxmlSyntaxHighlighter * h =*/// new TxmlSyntaxHighlighter(this, e->document()); 
 e->setAcceptRichText(false);
 e->insertPlainText(getValue());
 e->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

 c = e->textCursor(); c.setPosition(0, QTextCursor::MoveAnchor);
 e->setTextCursor(c);

 connect(e, SIGNAL(textChanged()), SLOT(onTextChanged()));
 return(e);
}
//--------------------------------------------------------------------------
void TdocElement::onTextChanged()
{
 QTextEdit * e = dynamic_cast<QTextEdit *>(sender());
 setValue(e->toPlainText());
 item->setText(getName() + ": " + getItemName());
}
//--------------------------------------------------------------------------
QStandardItemDoc * TdocElement::getItem()
{
 QList<TdocElement *>::iterator itr;
 item = new QStandardItemDoc();
 item->doc = this;
 if (getItemName().isEmpty()) item->setText(getName());
 else item->setText(getName() + ": " + getItemName());

 for(itr = child.begin(); itr != child.end(); ++itr)
 {
  QStandardItemDoc * child = (*itr)->getItem();
  if (child != NULL)
  {
   item->appendRow(child);
  }
 }

 return(item); 
}
//--------------------------------------------------------------------------
void TdocElement::onMenuPressed() 
{
 QString p(sender()->objectName());

// qDebug() << "TdocElement::onMenuPressed " << sender()->objectName();
// return;

 if (p == "up")  {if (parent != NULL) parent->upChild(this); emit(onTreeSetIndex(item->index()));}
 else if (p == "down"){if (parent != NULL) parent->downChild(this); emit(onTreeSetIndex(item->index()));}
 else
 {
  TdocElement * el = createElement(p);
  if (el != NULL)
  {
   addChild(el);
   item->appendRow(el->getItem());
   emit(onTreeSetIndex(el->item->index()));
  }
 }

 return;
}
//--------------------------------------------------------------------------
void TdocElement::onDelete()
{
// qDebug() << "delete 0 " << (int)parent;
  if (parent != NULL) parent->removeChild(this);
//qDebug() << "delete 1";
  delete(this);
//qDebug() << "delete 2";
 return;
}
//--------------------------------------------------------------------------
//void TdocElement::onCopy()
//{
// QModelIndexList list = ui->treeView->selectionModel()->selectedIndexes();
//}
//--------------------------------------------------------------------------
//void TdocElement::onPaste()
//{
//}
//--------------------------------------------------------------------------
//void TdocElement::onAction()
//{
// qDebug() << "onAction sender is " << sender()->objectName();
//}
//--------------------------------------------------------------------------
bool TdocElement::initElement(QDomElement * domElement) // ret = true - продолжить парсинг вглубину
{
// if (isText()) setValue(domElement->text());
// else
// {
  if (domElement->hasChildNodes())
  {
   if (domElement->childNodes().count() == 1 && domElement->firstChild().isText())
   {
    setValue(domElement->text());
    return(false);
   }
  }
  else
  {
   //setValue(domElement->text());
  }

 return(true);
}
//--------------------------------------------------------------------------
void TdocElement::initMap(QDomElement * domElement)
{
 QDomNamedNodeMap nm;

 nm = domElement->attributes();

 for(int i = 0; i < nm.count(); i++)
 {
  QDomNode node = nm.item(i);
  attribute.insert(node.nodeName(), node.nodeValue());
//qDebug() << node.nodeName() << " " << node.nodeValue();
 }
}
//--------------------------------------------------------------------------
bool TdocElement::saveElement(QDomDocument * doc, QDomElement * parent) // экспорт данных в QDomElement * parent
{
 bool ret(true); // ret = true - продолжить парсинг вглубину
 QList<TdocElement *>::iterator itr;
 QMap<QString, QString>::iterator itra;

 QDomElement e = doc->createElement(getName());
// e.setNodeValue(getValue());
 if (!getValue().isEmpty() && child.empty())
 {
  QDomText text = doc->createTextNode(getValue());
//  QDomText text;// = new QDomText();
//  text . setNodeValue(getValue());
  e.appendChild(text);
  ret = false;
 }

 if (child.size() == 1 && child.first()->getName() == "text")
 {
  QDomText text = doc->createTextNode(child.first()->getValue());
  e.appendChild(text);
  ret = false;
 }

 for(itra = attribute.begin(); itra != attribute.end(); ++itra)
 {
  e.setAttribute(itra.key(), itra.value());
 }

 parent->appendChild(e);

 if (ret)
 {
  for(itr = child.begin(); itr != child.end(); ++itr)
  {
   (*itr)->saveElement(doc, &e);
  } 
 }
 return(ret);
}
//--------------------------------------------------------------------------
QWidget * TformatTextElement::getWidget(QWidget * parent)
{
 rte = new TrichTextEdit(parent);

 //appendText(NULL);

 rte->setDocElement(this);

 QTextCursor c = rte->textEdit->textCursor(); c.setPosition(0, QTextCursor::MoveAnchor);
 rte->textEdit->setTextCursor(c);

 connect(rte, SIGNAL(changedCurrentDoc(TdocElement * )), SLOT(onChangedCurrentDoc(TdocElement *)));

 return(rte); 
}
//--------------------------------------------------------------------------
void TformatTextElement::onChangedCurrentDoc(TdocElement * doc)
{
qDebug() << __PRETTY_FUNCTION__;
 if (doc != NULL && doc->item != NULL) emit onTreeSetIndex(doc->item->index());
}
//--------------------------------------------------------------------------
void TformatTextElement::appendText(TdocElement * e)
{
// TODO : выпрямить алгоритм разбора вложенного форматирования
 QList<TdocElement *>::iterator itr;

 if (e == NULL) e = this;

 if (e->child.empty()/* || (e->child.size() == 1)*/)
 {
  if (e->isBlockText())
  {
   TblockTextElement * be = dynamic_cast<TblockTextElement *>(e);
   be->insertInto(rte->textEdit);   
  }
  else
  {
   rte->textEdit->insertPlainText(e->value);
   rte->textEdit->textCursor().insertBlock();
  }
 }
 else
 {
  for(itr = e->child.begin(); itr != e->child.end(); ++itr)
  {
   appendText(*itr);
  }
 }

}
//--------------------------------------------------------------------------
QWidget * TgenreElement::getWidget(QWidget * parent)
{
 int i(0);
 QWidget * w = new QWidget(parent);
 QGridLayout * l = new QGridLayout(w);
 QSpinBox * spin = new QSpinBox(w);

 spin->setMinimum(1); spin->setMaximum(100);
 spin->setValue(match);
 
 QString v(value.toUpper());
 QList<Tgenre>::iterator itr;
 cbo = new QComboBox(w);
 cbo->setEditable(false);
 cbo->clear();
 for(itr = genre.begin(); itr != genre.end(); ++itr)
 {
  cbo->addItem(itr->fname, itr->sname);
  if (v == itr->sname.toUpper()) { i = cbo->count() - 1; }
 }

 if (i != 0) cbo->setCurrentIndex(i);

 connect(cbo,  SIGNAL(currentIndexChanged(int)), SLOT(onCurrentIndexChanged(int)));
 connect(spin, SIGNAL(valueChanged(int)),        SLOT(onValueChanged(int)));

 l->addWidget(cbo, 0, 0);
 l->addWidget(spin, 0, 1);

 w->setLayout(l);
 return(w);
}
//--------------------------------------------------------------------------
void TgenreElement::onCurrentIndexChanged(int index)
{
 setValue(cbo->itemData(index).toString());
}
//--------------------------------------------------------------------------
void TgenreElement::onValueChanged(int index)
{
// TODO : save into attributes
 match = index;
}
//--------------------------------------------------------------------------
QWidget * TpersonElement::getWidget(QWidget * parent)
{
 TdocElement * ch;
 QWidget * w = new QWidget(parent);
 QGridLayout * l = new QGridLayout(w);

 QLineEdit * efirstname = new QLineEdit("", w); l->addWidget(efirstname, 0, 1);
 QLabel    * lfirstname = new QLabel("Firstname:"); l->addWidget(lfirstname, 0, 0);
 if ((ch = getChild("first-name")) != NULL)
 {
  efirstname->setText(ch->getValue());
  connect(efirstname, SIGNAL(textChanged(const QString &)), ch, SLOT(setValue(const QString &)));
 }
 else
 {
  efirstname->setEnabled(false);
 }

 QLineEdit * emiddlename = new QLineEdit("", w); l->addWidget(emiddlename, 2, 1);
 QLabel    * lmiddlename = new QLabel("Middlename:"); l->addWidget(lmiddlename, 2, 0);
 if ((ch = getChild("middle-name")) != NULL)
 {
  emiddlename->setText(ch->getValue());
  connect(emiddlename, SIGNAL(textChanged(const QString &)), ch, SLOT(setValue(const QString &)));
 }
 else
 {
  emiddlename->setEnabled(false);
 }

 QLineEdit * elastname = new QLineEdit("", w); l->addWidget(elastname, 3, 1);
 QLabel    * llastname = new QLabel("Lastname:"); l->addWidget(llastname, 3, 0);
 if ((ch = getChild("last-name")) != NULL)
 {
  elastname->setText(ch->getValue());
  connect(elastname, SIGNAL(textChanged(const QString &)), ch, SLOT(setValue(const QString &)));
 }
 else
 {
  elastname->setEnabled(false);
 }

 QLineEdit * enickname = new QLineEdit("", w); l->addWidget(enickname, 4, 1);
 QLabel    * lnickname = new QLabel("Nickname:"); l->addWidget(lnickname, 4, 0);
 if ((ch = getChild("nickname")) != NULL)
 {
  enickname->setText(ch->getValue());
  connect(enickname, SIGNAL(textChanged(const QString &)), ch, SLOT(setValue(const QString &)));
 }
 else
 {
  enickname->setEnabled(false);
 }

 QLineEdit * ehomepage = new QLineEdit("", w); l->addWidget(ehomepage, 5, 1);
 QLabel    * lhomepage = new QLabel("Homepage:"); l->addWidget(lhomepage, 5, 0);
 if ((ch = getChild("home-page")) != NULL)
 {
  ehomepage->setText(ch->getValue());
  connect(ehomepage, SIGNAL(textChanged(const QString &)), ch, SLOT(setValue(const QString &)));
 }
 else
 {
  ehomepage->setEnabled(false);
 }

 QLineEdit * eemail = new QLineEdit("", w); l->addWidget(eemail, 6, 1);
 QLabel    * lemail = new QLabel("Email:"); l->addWidget(lemail, 6, 0);
 if ((ch = getChild("email")) != NULL)
 {
  eemail->setText(ch->getValue());
  connect(eemail, SIGNAL(textChanged(const QString &)), ch, SLOT(setValue(const QString &)));
 }
 else
 {
  eemail->setEnabled(false);
 }

 QLineEdit * eid = new QLineEdit("", w); l->addWidget(eid, 7, 1);
 QLabel    * lid = new QLabel("Id:"); l->addWidget(lid, 7, 0);
 if ((ch = getChild("id")) != NULL)
 {
  eid->setText(ch->getValue());
  connect(eid, SIGNAL(textChanged(const QString &)), ch, SLOT(setValue(const QString &)));
 }
 else
 {
  eid->setEnabled(false);
 }

 w->setLayout(l);
 return(w);
}
//--------------------------------------------------------------------------
QStandardItemDoc * TpersonElement::getItem()
{
 QList<TdocElement *>::iterator itr;
 item = new QStandardItemDoc();
 item->doc = this;
 item->setText(getName());

// for(itr = child.begin(); itr != child.end(); ++itr)
// {
//  QStandardItem * child = (*itr)->getItem();
//  item->appendRow(child);
// }

 return(item);
}
//--------------------------------------------------------------------------
/*bool TblockTextElement::initElement(QDomElement * domElement)
{
 if (domElement->hasChildNodes())
 {
  if (domElement->childNodes().count() == 1 && domElement->firstChild().isText())
  {
   return(TdocElement::initElement(domElement));
  }
  else
  {
   QTextStream stm; QString str;
   stm.setString(&str, QIODevice::ReadWrite);
   domElement->save(stm, 0);
   setValue(stm.readAll());
   return(false);
  }
 }
 else
 {
  return(TdocElement::initElement(domElement));
 }
}*/
//--------------------------------------------------------------------------
QWidget * TdateElement::getWidget(QWidget * parent)
{
 QDate date;
 QString dstr("");
 QWidget *     w  = new QWidget(parent);
 QVBoxLayout * vb = new QVBoxLayout();
 QPushButton * b  = new QPushButton();
               de = new QDateEdit();

 b->setText(QDate::currentDate().toString("dd.MM.yyyy"));

 vb->addWidget(de);
 vb->addWidget(b);
 vb->addStretch(100);
 w->setLayout(vb);

 if (attribute.contains("value"))
 {
  dstr = attribute.value("value");
 }
 else
 {
  dstr = value;
 }

 date = QDate::fromString(dstr, "yyyy-MM-dd");
 de->setDate(date);
 de->setDisplayFormat("dd.MM.yyyy");

 connect(de, SIGNAL(dateChanged(const QDate &)), SLOT(onDateChanged(const QDate &)));
 connect(b,  SIGNAL(pressed()),                  SLOT(onDateChangedPressed()));

 return(w);
}
//--------------------------------------------------------------------------
void TdateElement::onDateChanged(const QDate & p)
{
 setValue ( p.toString("yyyy-MM-dd") );
 attribute.insert("value", value);
 item->setText(getName() + ": " + getItemName());
}
//--------------------------------------------------------------------------
void TdateElement::onDateChangedPressed()
{
 de->setDate(QDate::currentDate());
 onDateChanged(de->date());
}
//--------------------------------------------------------------------------
QWidget * Tlanguage::getWidget(QWidget * parent)
{
 int i(0);
 QString v(getValue().toLower());
 QList<Tlang>::iterator itr; 
 /*QComboBox **/ cbo = new QComboBox(parent);
 cbo->setEditable(false);
 cbo->clear();
 for(itr = lang.begin(); itr != lang.end(); ++itr)
 {
  cbo->addItem(itr->fname, itr->name2.toLower());
  if (v == itr->name2.toLower()) { i = cbo->count() - 1; }
 }

 if (i != 0) cbo->setCurrentIndex(i);

 connect(cbo, SIGNAL(currentIndexChanged(int)), SLOT(onCurrentIndexChanged(int)));

 return(cbo);
}
//--------------------------------------------------------------------------
void Tlanguage::onCurrentIndexChanged(int index)
{
 setValue(cbo->itemData(index).toString());
 item->setText(getName() + ": " + getItemName());

//qDebug() << value << getValue();
}
//--------------------------------------------------------------------------
QWidget * TimageElement::getWidget(QWidget * p)
{
 TdocElement * root;
 TbinaryElement * bin = NULL;
 QWidget * w = new QWidget(p);
 QGridLayout * gl = new QGridLayout();
 QVBoxLayout * vb = new QVBoxLayout();
 QList<TdocElement *>::iterator itr;
 QMap<QString, QString>::iterator itrm;
 QString fileName("");
 QLabel * limg;

 for(itrm = attribute.begin(); itrm != attribute.end(); ++itrm)
 {
//  qDebug() << itrm.key() << " " << itrm.value();
  if (itrm.key().indexOf("href") != -1) {fileName = itrm.value(); break;}
 }

 if (fileName.left(1) == "#")
 {
  fileName.remove(0, 1);      // remove first symbol '#' 

  root = parent;
  while(root->parent != NULL)
  {
   root = root->parent;
  }
  for(itr = root->child.begin(); itr != root->child.end(); ++itr)
  {
   if ((*itr)->getName() != "binary") continue;
   if (!(*itr)->attribute.contains("content-type")) continue;
   if ((*itr)->attribute.value("content-type").indexOf("image") == -1) continue;
   if ((*itr)->attribute.value("id") == fileName)
   {
    bin = dynamic_cast<TbinaryElement *>(*itr);
    break;
   }
  }
//  if (bin != NULL)
//  {
//   for(itrm = bin->attribute.begin(); itrm != bin->attribute.end(); ++itrm)
//   {
//    qDebug() << itrm.key() << " " << itrm.value();
//   }
//  }
 } // '#'
 else
 {
  // TODO: Where is a file ? 
 }

 if (bin != NULL)
 {

 }
 else
 {
  return(w);
 }
 int y(0);
 for(itrm = attribute.begin(); itrm != attribute.end(); ++itrm)
 {
  gl->addWidget(new QLabel(itrm.key() + ":"), y, 0); gl->addWidget(new QLabel(itrm.value()), y, 1);
  y++;
 }

 limg = new QLabel(w);
 //limg->setScaledContents(true);

 QPixmap* pix = new QPixmap();
 pix->loadFromData(bin->getData());

 limg->setPixmap(*pix);
 limg->setAlignment(Qt::AlignCenter);

 vb->addLayout(gl, 1);
 vb->addWidget(limg, 3);
 w->setLayout(vb);
 return(w);
}
//--------------------------------------------------------------------------
QWidget * TblockTextElement::getWidget(QWidget * parent)
{
 QList<TdocElement *>::iterator itr;
 QTextCursor c;

 //return(NULL);

 QTextEdit * e = new QTextEdit(parent);
 /*TxmlSyntaxHighlighter * h =*/ //new TxmlSyntaxHighlighter(this, e->document());
 e->setAcceptRichText(true);

// TODO : нужен алгоритм для прямой отработки вложенных тэгов

 if (child.empty()) e->insertPlainText(getValue());
 else
 for(itr = child.begin(); itr != child.end(); ++itr)
 {
  if ((*itr)->getName() == "emphasis" || (*itr)->getName() == "strong" ||
      (*itr)->getName() == "sub" || (*itr)->getName() == "sup" || (*itr)->getName() == "code")
  {
   (dynamic_cast<TfontStyleElement *>(*itr))->getFormat(&format);
  }
  c = e->textCursor(); c.setCharFormat(format);
  //e->textCursor().insertBlock();
  e->setTextCursor(c);
  
  e->insertPlainText((*itr)->getValue());
  QTextCursor ec(e->textCursor());
  ec.setCharFormat(QTextCharFormat());
  //ec.insertBlock();
//  e->setTextCursor(ec);
//  e->insertPlainText("* ! * ! *");
  //e->insertPlainText(" ");  //TODO : разделитель должен быть более интеллектуальным
 }

 c = e->textCursor(); c.setPosition(0, QTextCursor::MoveAnchor);
 e->setTextCursor(c);

 connect(e, SIGNAL(textChanged()), SLOT(onTextChanged()));

 return(e);
}
//--------------------------------------------------------------------------
void TblockTextElement::insertInto(QTextEdit * e)
{
 QTextCursor c;
 QList<TdocElement *>::iterator itr;

 if (child.empty())
 {
  e->insertPlainText(value);
  e->textCursor().insertBlock();
  return;
 }


 for(itr = child.begin(); itr != child.end(); ++itr)
 {
  if ((*itr)->getName() == "text"){e->insertPlainText((*itr)->getValue()); continue;}
  if ((*itr)->isFontStyle())
  {
   (dynamic_cast<TfontStyleElement *>(*itr))->getFormat(&format);
   c = e->textCursor(); c.setCharFormat(format);
   e->setTextCursor(c);
   e->insertPlainText((*itr)->getValue());
   QTextCursor ec(e->textCursor());
   ec.setCharFormat(QTextCharFormat());
   e->setTextCursor(ec);
  }
 }
 e->textCursor().insertBlock();
}
//--------------------------------------------------------------------------
void TblockTextElement::onTextChanged()
{
 QTextEdit * e = dynamic_cast<QTextEdit *>(sender());

 if (child.empty())
 {
  setValue(e->toPlainText());
 }
 else
 {
  QString str("");
  QTextDocument * doc = e->document();
  QTextBlock block;

  document2doc(doc, this);

// for(block = doc->begin(); block != doc->end(); block = block.next())
// {
//  str += block.text();
// }
// setValue(str);
 }

// qDebug() << str;
}
//--------------------------------------------------------------------------
QMenu * TpElement::getItemMenu()
{
 QMenu * menu = TdocElement::getItemMenu(); 

 QAction * a;

 menu->addSeparator();

 a = new QAction("convert to section", this); a->setData("convert_to_section"); a->setObjectName("convert_to_section"); menu->addAction(a);
 connect(a, SIGNAL(triggered()), this, SLOT(convert_to_section()));

 return(menu);
}
//--------------------------------------------------------------------------
void TpElement::convert_to_section()
{
 QList<TdocElement *>::iterator itr, itr2;
 TdocElement * body, * section, * title;
 //TdocElement * old_section;

 body = getParent("section")->parent; //getParent("body");
 if (body == NULL){qDebug() << "body is NULL"; return;}

 section = new TsectionElement();
 title   = new TtitleElement(); 

 //old_section == getParent("section");
 body->insertChild(section, getParent("section"));

 itr = parent->child.end(); --itr;
 for(; itr != parent->child.begin(); --itr)
 {
  if (*itr == this) {break; }
  section->child.push_front(*itr);
  parent->removeChild(*itr);
 }

 moveChild(this, title);
 section->child.push_front(title);

// tdocElement * s = getParent("section");
// int pos = body->childPosition(s);
// qDebug() << "s=" << s->getName() << " pos=" << pos;
 body->item->insertRow(body->childPosition(getParent("section")) + 1, section->getItem());
}
//--------------------------------------------------------------------------
TbinaryElement::TbinaryElement(const QString fileName) : TdocElement("binary", true) 
{
 QFile file(fileName);
 QFileInfo fileInfo(fileName);
 QString contentType;

 text = false;

 if (fileInfo.suffix().toLower() == "jpg" || fileInfo.suffix().toLower() == "jpeg")
 {
  contentType = "image/jpeg";
 }
 else
 contentType = "image/" + fileInfo.suffix().toLower();

 attribute.insert("content-type", contentType);
 attribute.insert("id", fileInfo.fileName());

 if (!file.exists()) return;

 if (!file.open(QIODevice::ReadOnly)) return;
 value = file.readAll();
 file.close();
}
//--------------------------------------------------------------------------
TbinaryElement::TbinaryElement(const QByteArray & other, const QString fileName) : TdocElement("binary", true),
value(other)
{
 attribute.insert("content-type", "image/png");
 attribute.insert("id", fileName);
}
//--------------------------------------------------------------------------
void TbinaryElement::setValue(const QString & p)
{
 QByteArray xcode("");
 xcode.append(p);
 value = QByteArray::fromBase64(xcode);
}
//--------------------------------------------------------------------------
QString TbinaryElement::getValue() const
{
 return (value.toBase64());
}
//--------------------------------------------------------------------------
bool TbinaryElement::initElement(QDomElement * domElement) // импорт данных из QDomElement *
{
 setValue(domElement->text());
 return(false);
}
//--------------------------------------------------------------------------
QWidget * TbinaryElement::getWidget(QWidget * p)
{
 QWidget * w = new QWidget(p);
 QGridLayout * gl = new QGridLayout();
 QVBoxLayout * vb = new QVBoxLayout();
 QMap<QString, QString>::iterator itrm;
 QString fileName("");
 QLabel * limg;

 int y(0);
 for(itrm = attribute.begin(); itrm != attribute.end(); ++itrm)
 {
  gl->addWidget(new QLabel(itrm.key() + ":"), y, 0); gl->addWidget(new QLabel(itrm.value()), y, 1);
  y++;
 }
 {
  QString s; s.setNum(value.size(), 10);
  gl->addWidget(new QLabel("size:"), y, 0); gl->addWidget(new QLabel(s), y, 1);
 }

 limg = new QLabel(w);

 if (attribute.contains("content-type"))
 if (attribute.value("content-type").indexOf("image") != -1)
 {
  QPixmap* pix = new QPixmap();
  pix->loadFromData(value);
  limg->setPixmap(*pix);
  limg->setAlignment(Qt::AlignCenter);
 }

 vb->addLayout(gl, 1);
 vb->addWidget(limg, 3);
 w->setLayout(vb);
 return(w);
}
//--------------------------------------------------------------------------
QString TbinaryElement::getItemName()
{
 if (attribute.contains("id"))
  return(attribute.value("id"));
 else
  return("");
}
//--------------------------------------------------------------------------
//bool TtextElement::saveElement(QDomDocument * doc, QDomElement * parent) // экспорт данных в QDomElement * parent
//{
// QDomElement e = doc->createElement(getName());
// QDomText text = doc->createTextNode(getValue());
// e.appendChild(text);
// parent->appendChild(e);

// return(false);
//}
//--------------------------------------------------------------------------
TdocElement * xml2doc(const QDomNode& node, TdocElement * root)
{
 TdocElement * ret = NULL;
// domElement.tagName().mid(1, domElement.tagName().length() - 2)
 if (root == NULL)
 {
  if(node.isElement())
  {
   QDomElement domElement = node.toElement();
   if(!domElement.isNull())
   {
    root = createElement (domElement.tagName());
    root->initMap(&domElement);
    ret  = root;
   }
  }
  else
  {
   return(ret);
  }
 }

 QDomNode domNode = node.firstChild();
 while(!domNode.isNull())
 {
  TdocElement * child;
  if (domNode.isElement())
  {
   QDomElement domElement = domNode.toElement();
   if (!domElement.tagName().isEmpty())
   {
    child = createElement (domElement.tagName());
    root->addChild(child);
    child->setParent(root);
    child->initMap(&domElement);
    //child->setValue(domElement.text()); 
    if (child->initElement(&domElement))
    {xml2doc(domNode, child);}
   }
  }
  else
  if (domNode.isText())
  {
   QDomText domText = domNode.toText();
//qDebug() << domText.nodeName();
   child = createElement ("text");
   root->addChild(child);
   child->setParent(root);
   //child->initMap(&domElement);
   child->setValue(domText.data());
   //child->initElement(&domElement);
  }
  domNode = domNode.nextSibling();
 }

 return(ret);
}
//--------------------------------------------------------------------------
TdocElement * createElement(const QString p)
{
 TdocElement * ret;

 if (p == "FictionBook")         {ret = new TFictionBookElement();}
 else if (p == "description")    {ret = new TdescriptionElement();}
 else if (p == "title-info")     {ret = new TtitleInfoElement();}
 else if (p == "src-title-info") {ret = new TsrcTitleInfoElement();}
 else if (p == "publish-info")   {ret = new TpublishInfoElement();}
 else if (p == "custom-info")    {ret = new TcustomInfoElement();}
 else if (p == "genre")          {ret = new TgenreElement();}
 else if (p == "author")         {ret = new TauthorElement();}
 else if (p == "first-name")     {ret = new TfirstNameElement();}
 else if (p == "last-name")      {ret = new TlastNameElement();}
 else if (p == "middle-name")    {ret = new TmiddleNameElement();}
 else if (p == "nickname")       {ret = new TnicknameElement();}
 else if (p == "email")          {ret = new TemailElement();}
 else if (p == "book-title")     {ret = new TbookTitleElement();}
 else if (p == "annotation")     {ret = new TannotationElement();}
 else if (p == "keywords")       {ret = new TkeywordsElement();}
 else if (p == "date")           {ret = new TdateElement();}
 else if (p == "coverpage")      {ret = new TcoverpageElement();}
 else if (p == "lang")           {ret = new TlangElement();}
 else if (p == "src-lang")       {ret = new TsrcLangElement();}
 else if (p == "translator")     {ret = new TtranslatorElement();}
 else if (p == "secquence")      {ret = new TsequenceElement();}
 else if (p == "document-info")  {ret = new TdocumentInfoElement();}
 else if (p == "src-url")        {ret = new TsrcUrlElement();}
 else if (p == "src-ocr")        {ret = new TsrcOcrElement();}
 else if (p == "id")             {ret = new TidElement();}
 else if (p == "version")        {ret = new TversionElement();}
 else if (p == "history")        {ret = new ThistoryElement();}
 else if (p == "book-name")      {ret = new TbookNameElement();}
 else if (p == "publisher")      {ret = new TpublisherElement();}
 else if (p == "city")           {ret = new TcityElement();}
 else if (p == "year")           {ret = new TyearElement();}
 else if (p == "body")           {ret = new TbodyElement();}
 else if (p == "section")        {ret = new TsectionElement();}
 else if (p == "p")              {ret = new TpElement();}
 else if (p == "poem")           {ret = new TpoemElement();}
 else if (p == "stanza")         {ret = new TstanzaElement();}
 else if (p == "v")              {ret = new TvElement();}
 else if (p == "subtitle")       {ret = new TsubtitleElement();}
 else if (p == "cite")           {ret = new TciteElement();}
 else if (p == "table")          {ret = new TtableElement();}
 else if (p == "empty-line")     {ret = new TemptyLineElement();}
 else if (p == "emphasis")       {ret = new TemphasisElement();}
 else if (p == "strong")         {ret = new TstrongElement();}
 else if (p == "sub")            {ret = new TsubElement();}
 else if (p == "sup")            {ret = new TsupElement();}
 else if (p == "code")           {ret = new TcodeElement();}
 else if (p == "title")          {ret = new TtitleElement();}
 else if (p == "subtitle")       {ret = new TsubtitleElement();}
 else if (p == "epigraph")       {ret = new TepigraphElement();}
 else if (p == "image")          {ret = new TimageElement();}
 else if (p == "binary")         {ret = new TbinaryElement();}
 else if (p == "text")           {ret = new TtextElement();}
 else ret = new TdocElement(p);

//qDebug() << "create " << p;

 return(ret);
}
//--------------------------------------------------------------------------
void document2doc(QTextDocument * document, TdocElement * root)
{
 QString str("");
 QTextBlock block;

 for(block = document->begin(); block != document->end(); block = block.next())
 {
  {QTextBlockFormat format = block.blockFormat();
   if (format.isBlockFormat()) qDebug() << "QTextBlockFormat isBlockFormat";
   if (format.isCharFormat()) qDebug() << "QTextBlockFormat isCharFormat";
  }

  {QTextCharFormat format = block.charFormat();
   if (format.isBlockFormat()) qDebug() << "QTextCharFormat isBlockFormat";
   if (format.isCharFormat()) qDebug() << "QTextCharFormat isCharFormat";
   if (format.hasProperty(QTextFormat::FontItalic)) qDebug() << "QTextCharFormat hasProperty";
   if (format.fontItalic())
   {qDebug() << "Italic";}
   if (format.fontWeight() == QFont::Bold) {}
  }
  
//  qDebug() << block.text();
 }
// setValue(str);

}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
